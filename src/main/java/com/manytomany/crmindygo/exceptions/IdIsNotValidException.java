package com.manytomany.crmindygo.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sige
 */
public class IdIsNotValidException extends RuntimeException {


    /**
     * Catch with Handler in Controller!
     */
    public IdIsNotValidException() {
    }

    /**
     * Constructs an instance of <code>IdIsNotValidException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public IdIsNotValidException(String msg) {
        super(msg);
    }
}
