/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Balazs
 */
public class SavingCourseToDatabaseFailed extends RuntimeException {

    static final Logger LOG = LoggerFactory.getLogger(SavingCourseToDatabaseFailed.class);
    
    /**
     * Creates a new instance of <code>SavingCourseToDatabaseFailed</code>
     * without detail message.
     */
    public SavingCourseToDatabaseFailed() {
    }

    /**
     * Constructs an instance of <code>SavingCourseToDatabaseFailed</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public SavingCourseToDatabaseFailed(String msg) {
        super(msg);
        LOG.error(msg);
    }
}
