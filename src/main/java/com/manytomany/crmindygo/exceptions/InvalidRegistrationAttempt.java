/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Balazs
 */
public class InvalidRegistrationAttempt extends RuntimeException {

    static final Logger LOG = LoggerFactory.getLogger(InvalidRegistrationAttempt.class);
    
    /**
     * Creates a new instance of <code>InvalidRegistrationAttempt</code> without
     * detail message.
     */
    public InvalidRegistrationAttempt() {
    }

    /**
     * Constructs an instance of <code>InvalidRegistrationAttempt</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidRegistrationAttempt(String msg) {
        super(msg);
        LOG.error(msg);
    }
}
