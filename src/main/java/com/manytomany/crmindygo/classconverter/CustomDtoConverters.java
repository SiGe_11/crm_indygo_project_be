package com.manytomany.crmindygo.classconverter;

import com.manytomany.crmindygo.dto.CourseDto;
import com.manytomany.crmindygo.dto.CourseTypeDto;
import com.manytomany.crmindygo.dto.CourseWithDateDto;
import com.manytomany.crmindygo.dto.CustomerDto;
import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.dto.responses.CourseModifiedResponse;
import com.manytomany.crmindygo.dto.responses.CustomerDashboardResponse;
import com.manytomany.crmindygo.dto.responses.DashboardCourseResponse;
import com.manytomany.crmindygo.dto.responses.DashboardCustomerResponse;
import com.manytomany.crmindygo.dto.responses.DashboardResponse;
import com.manytomany.crmindygo.dto.responses.NameEmailRoleResponse;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.user.Role;
import com.manytomany.crmindygo.entities.user.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Used for create custom dto-s from entities.
 *
 * @author sige
 */
@Component
public class CustomDtoConverters {

    @Autowired
    DtoToEntity dte;

    public NameEmailRoleResponse currentUserCreator(User user) {
        return new NameEmailRoleResponse(user.getName(), user.getEmail(), roleToStringConverter(user.getRoles()));
    }

    public NameEmailRoleResponse toRegisterUserCreator(String name, String email, String role) {
        return new NameEmailRoleResponse(name, email, role);
    }

    /**
     * Convert the roles list to a string list.
     *
     * @param roles
     * @return String
     */
    private static List<String> roleToStringConverter(List<Role> roles) {
        List<String> rolesString = new ArrayList<>();
        for (Role role : roles) {
            rolesString.add(role.getName());
        }
        return rolesString;
    }

    public List<CustomerDashboardResponse> customerDashBoardResponseCreator(List<CustomerDto> dtoList) {
        List<CustomerDashboardResponse> responseList = new ArrayList<>();
        for (CustomerDto customerDto : dtoList) {
            CustomerDashboardResponse response = new CustomerDashboardResponse(
                    customerDto.getName(),
                    customerDto.getId(),
                    customerDto.getEmail(),
                    customerDto.getStatus());
            responseList.add(response);
        }
        return responseList;
    }

//    public List<CourseDashboardResponse> courseDashBoardResponseCreator (List<CourseDto> dtoList) {
//        List<CourseDashboardResponse> responseList = new ArrayList<>();
//        for (CourseDto dto : dtoList) {
//            List<String> type = courseTypesToString(dto.getTypes());
//            CourseDashboardResponse response = new CourseDashboardResponse(type, dto.getName(), dto.dateStart, dateFinish, location, id);
//        }
//        return responseList;
//    }
    public DashboardResponse dashboardResponseCreator(List<CustomerDto> customerDtos, int howManyClientPages, List<CourseWithDateDto> courseDtos, int howManyCoursesPage) {
        List<CustomerDashboardResponse> customers = customerDashBoardResponseCreator(customerDtos);
        return new DashboardResponse(customers, howManyClientPages, courseDtos, howManyCoursesPage);
    }
    
    public DashboardCourseResponse dashboardCourseResponseCreator(List<CourseWithDateDto> courseDtos, int howManyCoursesPage) {
        return new DashboardCourseResponse(courseDtos, howManyCoursesPage);
    }
    
    public DashboardCustomerResponse dashboardCustomerResponseCreator(List<CustomerDto> customerDtos, int howManyClientPages) {
        List<CustomerDashboardResponse> customers = customerDashBoardResponseCreator(customerDtos);
        return new DashboardCustomerResponse(customers, howManyClientPages);
    }

    public List<String> courseTypesToString(List<CourseType> types) {
        List<String> typeStrings = new ArrayList<>();
        for (CourseType type : types) {
            typeStrings.add(type.getText());
        }
        return typeStrings;
    }

    public CourseWithDateDto convertCourseToCourseWithDateDto(Course course, Date dateStart, Date dateFinish) {
        List<String> type = courseTypesToString(course.getTypes());
        String modifiedLocation = returnModifiedLocation(course);
        CourseWithDateDto dto = new CourseWithDateDto(type, course.getName(), dateStart, dateFinish, modifiedLocation, course.getId(), null, null, null);
        return dto;
    }

    public String returnModifiedLocation(Course course) {
        return course.getCourseEvents().get(0).getLocation().getAddress().getZip()
                + " " + course.getCourseEvents().get(0).getLocation().getAddress().getCity();
    }

}
