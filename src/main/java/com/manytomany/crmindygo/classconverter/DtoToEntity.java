package com.manytomany.crmindygo.classconverter;

import com.manytomany.crmindygo.dto.AddressDto;
import com.manytomany.crmindygo.dto.CourseDto;
import com.manytomany.crmindygo.dto.CourseEventDto;
import com.manytomany.crmindygo.dto.CourseTypeDto;
import com.manytomany.crmindygo.dto.CustomerDto;
import com.manytomany.crmindygo.dto.LocationDto;
import com.manytomany.crmindygo.dto.TrainerDto;
import com.manytomany.crmindygo.dto.responses.CourseModifiedResponse;
import com.manytomany.crmindygo.dto.responses.CourseEventModifiedResponse;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.Customer;
import com.manytomany.crmindygo.entities.Location;
import com.manytomany.crmindygo.entities.user.TrainerUser;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Convert a DTO to an Entity with Doser.
 *
 * @author sige, Márta
 */
@Component
public class DtoToEntity {

    @Autowired
    DtoToEntity dte;
    @Autowired
    DozerBeanMapper mapper;

    public Customer customerConverter(CustomerDto dto) {
        //return new Customer(dto);
        Customer mapped = mapper.map(dto, Customer.class, "customerSaveMapping");
        return mapped;
    }
    
    public Customer customerConverterWithId(CustomerDto dto) {
        Customer mapped = mapper.map(dto, Customer.class);
        return mapped;
    }

    public CustomerDto customerBackConverter(Customer customer) {
        CustomerDto mapped = mapper.map(customer, CustomerDto.class);
        return mapped;
    }

    public List<CustomerDto> customerListBackConverter(List<Customer> customerList) {
        List<CustomerDto> reConvertered = new ArrayList<>();
        for (Customer customer : customerList) {
            CustomerDto dto = dte.customerBackConverter(customer);
            reConvertered.add(dto);
        }
        return reConvertered;
    }

    public Course courseConverter(CourseDto dto) {
        Course mapped = mapper.map(dto, Course.class);
        return mapped;
    }
    
    public Course courseConverterWithId(CourseDto dto) {
        Course mapped = mapper.map(dto, Course.class);
        return mapped;
    }

    public CourseDto courseBackConverter(Course course) {
        CourseDto mapped = mapper.map(course, CourseDto.class);
        return mapped;
    }

    public CourseModifiedResponse courseBackConverterForSearchById(Course course) {
        CourseModifiedResponse mapped = mapper.map(course, CourseModifiedResponse.class, "courseSearchById");
        return mapped;
    }

    public CourseType courseTypeConverter(CourseTypeDto dto) {
        CourseType mapped = mapper.map(dto, CourseType.class);
        return mapped;
    }

    public CourseTypeDto courseTypeBackConverter(CourseType courseType) {
        CourseTypeDto mapped = mapper.map(courseType, CourseTypeDto.class);
        return mapped;
    }

    public CourseEvent courseEventConverter(CourseEventDto dto) {
        CourseEvent mapped = mapper.map(dto, CourseEvent.class);
        return mapped;
    }

    public CourseEventDto courseEventBackConverter(CourseEvent courseEvent) {
        CourseEventDto mapped = mapper.map(courseEvent, CourseEventDto.class);
        return mapped;
    }
        
    public CourseEventModifiedResponse courseEventBackConverterForSearchById(CourseEvent courseEvent) {
        CourseEventModifiedResponse mapped = mapper.map(courseEvent, CourseEventModifiedResponse.class);
        return mapped;
    }

    public TrainerUser trainerConverter(TrainerDto dto) {
        TrainerUser mapped = mapper.map(dto, TrainerUser.class);
        return mapped;
    }

    public TrainerDto trainerBackConverter(TrainerUser trainer) {
        TrainerDto mapped = mapper.map(trainer, TrainerDto.class);
        return mapped;
    }

    public Address addressConverter(AddressDto dto) {
        Address mapped = mapper.map(dto, Address.class);
        return mapped;
    }
    
    public AddressDto addressBackConverter(Address address) {
        AddressDto mapped = mapper.map(address, AddressDto.class);
        return mapped;
    }
    
    public Location locationConverter(LocationDto dto) {
        Location mapped = mapper.map(dto, Location.class);
        return mapped;
    }
    
    public LocationDto locationBackConverter(Location location) {
        LocationDto mapped = mapper.map(location, LocationDto.class);
        return mapped;
    }

    

}
