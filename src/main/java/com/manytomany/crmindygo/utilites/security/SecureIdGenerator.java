package com.manytomany.crmindygo.utilites.security;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author sige
 */
@Component
public class SecureIdGenerator {

    static final Logger LOG = LoggerFactory.getLogger(SecureIdGenerator.class);
    
    /**
     * Generate a secure random ID.
     * 
     * @return HexString
     * @throws NoSuchAlgorithmException 
     */
    public static String secureId() throws NoSuchAlgorithmException{
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            LOG.info("New random ID generated.");
            return Long.toHexString(sr.nextLong());            
    }
    
}
