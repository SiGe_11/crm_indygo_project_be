package com.manytomany.crmindygo.utilites.mailsender;

import com.manytomany.crmindygo.dto.Mail;

/**
 *
 * @author sige
 */

public interface MailService {
    public void sendEmail(Mail mail);
}