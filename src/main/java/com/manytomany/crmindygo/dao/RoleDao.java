/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dao;

import com.manytomany.crmindygo.entities.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Balazs
 */
public interface RoleDao extends JpaRepository<Role, String> {
    
    Role findByName(String name);
    
}
