/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dao;

import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.Customer;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author TANULO
 */
public interface CustomerDao extends JpaRepository<Customer, String> {
    
    List<Customer> findById(String id);
    
    List<Customer> findAll();
    
}
