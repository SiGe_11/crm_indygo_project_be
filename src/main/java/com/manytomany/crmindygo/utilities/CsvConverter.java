package com.manytomany.crmindygo.utilities;

import com.manytomany.crmindygo.dto.CustomerDto;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author sige
 */
@Component
public class CsvConverter {
    //Egyelőre csak CustomerDTO, TODO generic T-t hozzáadni.
    //TODO adatokat is megírni, hogy mit kapjon pontosan, ez csak teszt
    //FONTOS! Egyelőre a tomcat/bin mappájába menti. TODO letölthetővé tenni és törölni a kész csv-t-
    public void writeCsv(List<CustomerDto> resultList, String filename){
        CSVWriter writer;
        List<String[]> resultListInString = new ArrayList<>();
        for (CustomerDto cdto : resultList) {
            String[] oneLine = new String[]{
            cdto.getName(),cdto.getCity(),cdto.getBirthPlace()};
            resultListInString.add(oneLine);
        }
        
        try {
            writer = new CSVWriter(new FileWriter(filename), '\t');
            writer.writeAll(resultListInString);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(CsvConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
}
