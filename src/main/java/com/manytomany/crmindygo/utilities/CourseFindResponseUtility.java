/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.utilities;

import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Márta
 */
public class CourseFindResponseUtility {
    
    public static List<CourseFindResponse> createCourseFindResponse(List<Course> courses) {
        List<CourseFindResponse> responses=new ArrayList<>();
        for (Course course : courses) {
            CourseFindResponse response=new CourseFindResponse();
            List<CourseType> types=course.getTypes();
            List<String> stringList=new ArrayList<>();
            for (CourseType type : types) {
                String text = type.getText();
                stringList.add(text);
            }
            String[] stringArray=new String[stringList.size()];
            stringArray=(String[])stringList.toArray(stringArray);
            response.type=stringArray;
            response.name=course.getName();
            List<CourseEvent> courseEvents = course.getCourseEvents();
            Date dateStart=null;
            Date dateFinish=null;
            for (int i = 0; i < courseEvents.size(); i++) {
                if(dateStart==null&&courseEvents.get(i).getDateStart()!=null)
                        dateStart=courseEvents.get(i).getDateStart();
                if(dateFinish==null&&courseEvents.get(i).getDateFinish()!=null)
                        dateFinish=courseEvents.get(i).getDateFinish();
                if(dateStart!=null&&courseEvents.get(i).getDateStart().before(dateStart))
                        dateStart=courseEvents.get(i).getDateStart();
                if(dateFinish!=null&&courseEvents.get(i).getDateFinish().after(dateFinish))
                        dateFinish=courseEvents.get(i).getDateFinish();
            }
            if(dateStart!=null)response.dateStart=dateStart.toString();
            if(dateFinish!=null)response.dateFinish=dateFinish.toString();
            for (int i = 0; i < courseEvents.size(); i++) {
                if(courseEvents.get(i).getLocation()!=null&&
                        courseEvents.get(i).getLocation().getAddress()!=null&&
                        courseEvents.get(i).getLocation().getAddress().getCity()!=null){
                    response.location=courseEvents.get(i).getLocation().getAddress().getCity();
                    break;
                }
            }
            response.id=course.getId();
            responses.add(response);
        }
        return responses;
    }
}
