/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.utilities;

import com.manytomany.crmindygo.manipulator.SimpleQuiryCourse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Márta
 */
public class DateUtility {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SimpleQuiryCourse.class);

    static final SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");
    static final SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
    static final SimpleDateFormat format3 = new SimpleDateFormat("yyyy. MM. dd");
    
    public static Date toDate(String searchExpression) {
        Date parsed = null;
        try {
            parsed = format1.parse(searchExpression);
        } catch (ParseException ex) {
            LOG.debug("Could not parse date with format1");
        }
        if (parsed == null) {
            try {
                parsed = format2.parse(searchExpression);
            } catch (ParseException ex) {
                LOG.debug("Could not parse date with format2");
            }
        }
        if (parsed == null) {
            try {
                parsed = format3.parse(searchExpression);
            } catch (ParseException ex) {
                LOG.debug("Could not parse date with format3");
            }
        }
        return parsed;
    }
    
}
