package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.CustomerDto;
import com.manytomany.crmindygo.entities.Customer;
import com.manytomany.crmindygo.entities.Customer_;
import com.manytomany.crmindygo.dao.CustomerDao;
import com.manytomany.crmindygo.dto.responses.CustomerCompoundFindResponse;
import com.manytomany.crmindygo.dto.responses.NameIdEmailStatus;
import com.manytomany.crmindygo.utilities.CsvConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sige, Tomi, Márta
 */
@Component
public class CustomerManipulator {

    @Autowired
    CustomerDao cd;

    @PersistenceContext
    EntityManager em;

    @Autowired
    DtoToEntity dte;
    
    @Autowired
    CustomerManipulator cuman;
    
    @Autowired
    CsvConverter csConv;

    /**
     * Save a Customer entity in the database.
     *
     * @param customer
     */
    @Transactional
    public void saveCustomerToDb(Customer customer) {
        em.persist(customer);
    }

    @Transactional
    public Customer modifyCustomer(Customer customer) {
        em.merge(customer);
        return customer;
    }
    
    //Archive Customer instead of deleting
    @Transactional
    public void deleteCustomer(CustomerDto dto) {
        Customer customer = em.find(Customer.class, dto.getId());
        customer.setOtherInformation("archived");
        em.merge(customer);
    }

    /**
     * Search for customer in all their fields.
     * @param searchExp
     * @param maxResult
     * @param page
     * @return List<CustomerDto>
     * 
     * Archived Customers are not returned by the search
     */
    public CustomerCompoundFindResponse simpleFindCustomer(String searchExp, int page, int maxResult) {
        int firstResult = ((page - 1) * maxResult);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Customer> cQuery = cb.createQuery(Customer.class);
        Root<Customer> c = cQuery.from(Customer.class);
        String searchExpWLike = "%"+searchExp+"%";      
        //TODO KERESNI ADRESSBEN!
        cQuery.select(c).where(cb.or(cb.like(c.get(Customer_.birthPlace), searchExpWLike),
                cb.like(c.get(Customer_.otherInformation), searchExpWLike),
                cb.like(c.get(Customer_.education), searchExpWLike),
                cb.like(c.get(Customer_.email), searchExpWLike),
                cb.like(c.get(Customer_.job), searchExpWLike),
                cb.like(c.get(Customer_.name), searchExpWLike),
                cb.like(c.get(Customer_.status), searchExpWLike)));
        if (searchExp.equals("*")) {
            cd.findAll(); // EZT ELLENŐRIZNI
        }
        List<Customer> resultList = em.createQuery(cQuery).setFirstResult(firstResult)
                .setMaxResults(maxResult).getResultList();        
        List<NameIdEmailStatus> responseResultList = new ArrayList<>();
        for (Customer customer : resultList) {
            //Archived Customers are not returned by the search
            if(customer.getOtherInformation()==null){
                responseResultList.add(new NameIdEmailStatus(customer.getName(), 
                        customer.getId(), customer.getEmail(), customer.getStatus()));
            }else if(!customer.getOtherInformation().equals("archived")){
                responseResultList.add(new NameIdEmailStatus(customer.getName(), 
                        customer.getId(), customer.getEmail(), customer.getStatus()));
            }
        }
        return new CustomerCompoundFindResponse(responseResultList, cuman.countCustomerPages(maxResult));
    }

    //Archived Customers are not returned by the search
    public CustomerCompoundFindResponse compoundFindCustomer(String name, String email, String status,
                    int page, int maxResult){
        int firstResult = ((page - 1) * maxResult);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Customer> cQuery = cb.createQuery(Customer.class);
        Root<Customer> c = cQuery.from(Customer.class);
        List<Predicate> preds = new ArrayList<>();
        if (!name.equals("*")) {
            preds.add(cb.like(c.get(Customer_.name), "%"+name+"%"));
        }
        if (!email.equals("*")) {
            preds.add(cb.like(c.get(Customer_.email), "%"+email+"%"));
        }
        if (!status.equals("*")) {
            preds.add(cb.like(c.get(Customer_.status), "%"+status+"%"));
        }
        //Archived Customers are not returned by the search
        preds.add(cb.notEqual(c.get(Customer_.otherInformation), "archived"));
        Predicate[] predicates = new Predicate[preds.size()];
        predicates = (Predicate[]) preds.toArray(predicates);
        cQuery.select(c).where(cb.and(predicates));
        List<Customer> resultList = em.createQuery(cQuery).setFirstResult(firstResult)
                .setMaxResults(maxResult).getResultList();
        List<NameIdEmailStatus> responseResultList = new ArrayList<>();
        for (Customer customer : resultList) {
            responseResultList.add(new NameIdEmailStatus(customer.getName(), customer.getId(), 
                                    customer.getEmail(), customer.getStatus()));
        }
        
        return new CustomerCompoundFindResponse(responseResultList, cuman.countCustomerPages(maxResult));
    }

    //Archived Customers are returned by the search
    public CustomerDto findCustomerById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Customer> cQuery = cb.createQuery(Customer.class);
        Root<Customer> c = cQuery.from(Customer.class);
        String searchExpWLike = id;
        cQuery.select(c).where(cb.equal(c.get(Customer_.id), searchExpWLike));
        return  dte.customerBackConverter(em.createQuery(cQuery).getSingleResult());
    }

    
    /**
     * List of all customers.
     * @return List
     * 
     * Archived Customers are returned by the search
     */
    public List<CustomerDto> listOfAllCustomers() {
        return dte.customerListBackConverter(em.createQuery("SELECT c FROM Customer c").getResultList());
    }

    public int countCustomerPages(int resultsPerPage) {
        long numberOfCustomers = (long) em.createQuery("SELECT COUNT(c.name) FROM Customer c").getSingleResult();
        return (int) Math.ceil(((float)numberOfCustomers) / resultsPerPage);
    }
    
    //Teszt alatt, CSV be lementi az összes customert.
    public void allCustomersToCsv(){
        csConv.writeCsv(dte.customerListBackConverter(em.createQuery("SELECT c FROM Customer c").getResultList()), "teszt.csv");
    }

}
