package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.dto.RegistrationDto;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.user.AdminUser;
import com.manytomany.crmindygo.entities.user.AssistantUser;
import com.manytomany.crmindygo.entities.user.TrainerUser;
import com.manytomany.crmindygo.entities.user.User;
import com.manytomany.crmindygo.exceptions.InvalidRegistrationAttempt;
import com.manytomany.crmindygo.exceptions.UserAlreadyExists;
import com.manytomany.crmindygo.dao.RoleDao;
import com.manytomany.crmindygo.dao.UserDao;
import com.manytomany.crmindygo.dto.responses.TrainersResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sige, Balazs
 */
@Component
public class UserManipulator {

    private static final Logger LOG = LoggerFactory.getLogger(UserManipulator.class);
    
    @PersistenceContext
    EntityManager em;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserDao userDao;

    @Autowired
    UserManipulator uman;

    @Autowired
    RoleDao roleDao;

    @Autowired
    InvitationEmailManipulator iem;
    
    @Transactional
    public String saveUserToDb(RegistrationDto registrationDto, String role) {
        String registrationLinkId = registrationDto.getGeneratedId();
        if (usernameExists(registrationDto.getUsername())) {
            throw new UserAlreadyExists("UserAlreadyExists");
        }
        if (role.equals("ROLE_ADMIN")) {
            String id = uman.saveAdminUserToDb(registrationDto);
            iem.deleteValidationLink(registrationLinkId);
            return id;
        } else if (role.equals("ROLE_TRAINER")) {
            String id = uman.saveTrainerUserToDb(registrationDto);
            iem.deleteValidationLink(registrationLinkId);
            return id;
        } else if (role.equals("ROLE_ASSISTANT")) {
            String id = uman.saveAssistantUserToDb(registrationDto);
            iem.deleteValidationLink(registrationLinkId);
            return id;
        } else {
            throw new InvalidRegistrationAttempt("InvalidRegistrationAttempt");
        }
    }
    
    @Transactional
    public User currentUser(String username){
        return userDao.findByUsername(username);
    }


    @Transactional
    public String saveAdminUserToDb(RegistrationDto registrationDto) {
        AdminUser admin = new AdminUser();
        admin.setCity(registrationDto.getAddress().getCity());
        admin.setStreet(registrationDto.getAddress().getStreet());
        admin.setNumber(registrationDto.getAddress().getNumber());
        admin.setZip(registrationDto.getAddress().getZip());
        admin.setGender(registrationDto.getGender());
        admin.setName(registrationDto.getName());
        admin.setTelephone(registrationDto.getTelephone());
        admin.setUsername(registrationDto.getUsername());
        admin.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        admin.setEmail(registrationDto.getEmail());
        admin.setRoles(Arrays.asList(roleDao.findByName("ROLE_ADMIN")));
        admin.setEnabled(true);
        userDao.save(admin);
        return admin.getId();
    }

    @Transactional
    public String saveTrainerUserToDb(RegistrationDto registrationDto) {
        TrainerUser trainer = new TrainerUser();
        trainer.setCity(registrationDto.getAddress().getCity());
        trainer.setStreet(registrationDto.getAddress().getStreet());
        trainer.setNumber(registrationDto.getAddress().getNumber());
        trainer.setZip(registrationDto.getAddress().getZip());
        trainer.setGender(registrationDto.getGender());
        trainer.setName(registrationDto.getName());
        trainer.setTelephone(registrationDto.getTelephone());
        trainer.setUsername(registrationDto.getUsername());
        trainer.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        trainer.setEmail(registrationDto.getEmail());
        trainer.setRoles(Arrays.asList(roleDao.findByName("ROLE_TRAINER")));
        trainer.setEnabled(true);
        userDao.save(trainer);
        if (registrationDto.getNotAcceptedLocations() != null) {
            uman.addTrainerEntityToNonAcceptedLocationEntity(registrationDto, trainer.getId());
        }
        return trainer.getId();
    }

    @Transactional
    public String saveAssistantUserToDb(RegistrationDto registrationDto) {
        AssistantUser assistant = new AssistantUser();
        assistant.setCity(registrationDto.getAddress().getCity());
        assistant.setStreet(registrationDto.getAddress().getStreet());
        assistant.setNumber(registrationDto.getAddress().getNumber());
        assistant.setZip(registrationDto.getAddress().getZip());
        assistant.setGender(registrationDto.getGender());
        assistant.setName(registrationDto.getName());
        assistant.setTelephone(registrationDto.getTelephone());
        assistant.setUsername(registrationDto.getUsername());
        assistant.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        assistant.setEmail(registrationDto.getEmail());
        assistant.setRoles(Arrays.asList(roleDao.findByName("ROLE_ASSISTANT")));
        assistant.setEnabled(true);
        userDao.save(assistant);
        return assistant.getId();
    }
    
    
    @Transactional
    public void addTrainerEntityToNonAcceptedLocationEntity(RegistrationDto registrationDto, String trainerId) {
        List<String> nonAcceptedAddressesFromUser = registrationDto.getNotAcceptedLocations();
        TrainerUser trainerFromDb = em.find(TrainerUser.class, trainerId);
        List<Address> addressList = new ArrayList<>();
        for (String addressId : nonAcceptedAddressesFromUser) {
            Address address = em.find(Address.class, addressId);
            address.getTrainersNotAcceptingThisAddress().add(trainerFromDb);
            addressList.add(address);
        }
        trainerFromDb.setNonAcceptedLocations(addressList);
    }

    @Transactional
    public List<TrainersResponse> getTrainers() {
        List<TrainerUser> trainersFromDb = (List<TrainerUser>) em.createQuery("SELECT t FROM TrainerUser t")
                .getResultList();
        List<TrainersResponse> trainersResponseList = new ArrayList<>();
        for (TrainerUser oneTrainerFromDb : trainersFromDb) {
            trainersResponseList.add(new TrainersResponse(oneTrainerFromDb.getId(), oneTrainerFromDb.getName()));
        }
        return trainersResponseList;
    }

    private boolean usernameExists(String username) {
        return userDao.findByUsername(username) != null;
    }

}
