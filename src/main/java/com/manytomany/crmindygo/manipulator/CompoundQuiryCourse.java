/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.utilities.DateUtility;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.Address_;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseEvent_;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.CourseType_;
import com.manytomany.crmindygo.entities.Course_;
import com.manytomany.crmindygo.entities.Location;
import com.manytomany.crmindygo.entities.Location_;
import com.manytomany.crmindygo.utilities.CourseFindResponseUtility;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Márta
 */
@Component
public class CompoundQuiryCourse {
    
    @PersistenceContext
    EntityManager em;

    @Autowired
    DtoToEntity dte;
    
    @Transactional(readOnly = true)
    public List<CourseFindResponse> findCourse(String type, String name, 
            String startBefore, String startAfter, String finishBefore, String finishAfter, 
            String location, int page, int maxResult) {
        int firstResult = ((page - 1) * maxResult);
        Date dateStartBefore = DateUtility.toDate(startBefore);
        Date dateStartAfter = DateUtility.toDate(startAfter);
        Date dateFinishBefore = DateUtility.toDate(finishBefore);
        Date dateFinishAfter = DateUtility.toDate(finishAfter);
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Course> criteriaQuery = criteriaBuilder.createQuery(Course.class);
        Root<Course> root = criteriaQuery.from(Course.class);
        ListJoin<Course, CourseEvent> courseEventJoined = root.join(Course_.courseEvents, JoinType.LEFT);
        Join<CourseEvent, Location> locationJoined = courseEventJoined.join(CourseEvent_.location, JoinType.LEFT);
        Join<Location,Address> addressJoined = locationJoined.join(Location_.address, JoinType.LEFT);
        ListJoin<Course, CourseType> courseTypeJoined = root.join(Course_.types, JoinType.LEFT);
        List<Predicate> preds = new ArrayList<>();
        if (!type.equals("")) {
            preds.add(criteriaBuilder.like(courseTypeJoined.get(CourseType_.text), "%"+type+"%"));
        }
        if (!name.equals("")) {
            preds.add(criteriaBuilder.like(root.get(Course_.name), "%"+name+"%"));
        }
        if (!location.equals("")) {
            preds.add(criteriaBuilder.like(addressJoined.get(Address_.city), "%"+location+"%"));
        }
        if (dateStartBefore!=null) {
            preds.add(criteriaBuilder.lessThan(courseEventJoined.get(CourseEvent_.dateStart), dateStartBefore));
        }
        if (dateStartAfter!=null) {
            preds.add(criteriaBuilder.greaterThan(courseEventJoined.get(CourseEvent_.dateStart), dateStartAfter));
        }
        if (dateFinishBefore!=null) {
            preds.add(criteriaBuilder.lessThan(courseEventJoined.get(CourseEvent_.dateFinish), dateFinishBefore));
        }
        if (dateFinishAfter!=null) {
            preds.add(criteriaBuilder.greaterThan(courseEventJoined.get(CourseEvent_.dateFinish), dateFinishAfter));
        }
        Predicate[] predicates=new Predicate[preds.size()];
        predicates = (Predicate[]) preds.toArray(predicates);
        criteriaQuery.select(root).where(criteriaBuilder.and(predicates));
        List<Course> resultList= em.createQuery(criteriaQuery).setFirstResult(firstResult)
                .setMaxResults(maxResult).getResultList();
        return CourseFindResponseUtility.createCourseFindResponse(resultList);
    }
}
