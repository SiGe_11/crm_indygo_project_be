package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.AddressDto;
import com.manytomany.crmindygo.dto.responses.AddressesResponse;
import com.manytomany.crmindygo.dto.responses.StatusResponse;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.Status;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Balazs
 */
@Component
public class EnumManipulator {
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    DtoToEntity dte;

    @Transactional
    public List<StatusResponse> returnStatuses() {
        List<String> statuses = em.createQuery("SELECT s.status FROM Status s").getResultList();
        List<StatusResponse> response = new ArrayList<>();
        for (String status : statuses) {
            response.add(new StatusResponse(status));
        }
        return response;
    }

    @Transactional
    public AddressesResponse returnAddresses() {
        List<Address> addressesFromDb = em.createQuery("SELECT a FROM Address a").getResultList();
        AddressesResponse response = new AddressesResponse();
        for (Address address : addressesFromDb) {
            response.getAddresses().add(new AddressDto(address.getZip(), address.getCity(), address.getStreet(), address.getNumber()));
        }
        return response;
    }
    
    
 
}
