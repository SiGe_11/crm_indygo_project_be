///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.manytomany.crmindygo.manipulator;
//
//import com.manytomany.crmindygo.classconverter.CustomDtoConverters;
//import com.manytomany.crmindygo.classconverter.DtoToEntity;
//import com.manytomany.crmindygo.dao.UserDao;
//import com.manytomany.crmindygo.dto.CourseWithDateDto;
//import com.manytomany.crmindygo.dto.CustomerDto;
//import com.manytomany.crmindygo.dto.responses.DashboardCourseResponse;
//import com.manytomany.crmindygo.dto.responses.DashboardCustomerResponse;
//import com.manytomany.crmindygo.dto.responses.DashboardResponse;
//import com.manytomany.crmindygo.entities.user.Role;
//import com.manytomany.crmindygo.entities.user.User;
//import java.util.ArrayList;
//import java.util.List;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// *
// * @author Balazs
// */
//@Component
//public class DashboardManipulator {
//
//    @PersistenceContext
//    EntityManager em;
//
//    @Autowired
//    DtoToEntity dte;
//
//    @Autowired
//    CustomDtoConverters cdc;
//
//    @Autowired
//    CustomerManipulator cuman;
//
//    @Autowired
//    CourseManipulator coman;
//
//    @Autowired
//    UserDao ud;
//
//    @Transactional
//    public DashboardResponse returnDashboardData() {
//        
//        int resultsPerPage = 5; //TODO adatbázisból elkérni ha már lesz ilyen entity (és adminfelület)
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String username = auth.getName();
//        User user = ud.findByUsername(username);
//        List<CustomerDto> customers = new ArrayList<>();
//        List<CourseWithDateDto> courses = new ArrayList<>();
//        int howManyClientPages = 0;
//        int howManyCoursePage = 0;
//        
//        if (user.getRoles().contains(new Role("ROLE_ADMIN")) || user.getRoles().contains(new Role("ROLE_ASSISTANT"))) {
//            customers = cuman.simpleFindCustomer("", 1, resultsPerPage);
//            howManyClientPages = cuman.countCustomerPages(resultsPerPage);
//            courses = coman.listOfAllCoursesWithDates(resultsPerPage, 1);
//            howManyCoursePage = coman.countAllCoursePages(resultsPerPage);
//        } else {
//            courses = coman.listOfMyCoursesWithDates(user, resultsPerPage, 1);
//            howManyCoursePage = coman.countMyCoursePages(user, resultsPerPage);
//        }
//        
//        DashboardResponse response = cdc.dashboardResponseCreator(customers, howManyClientPages, courses, howManyCoursePage);
//        return response;
//    }
//
//    @Transactional
//    public DashboardCourseResponse returnDashboardCourseData(int maxPerPage, int pageNumber) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String username = auth.getName();
//        User user = ud.findByUsername(username);
//        List<CourseWithDateDto> courses = new ArrayList<>();
//        int howManyCoursePage = 0;
//        
//        if (user.getRoles().contains(new Role("ROLE_ADMIN")) || user.getRoles().contains(new Role("ROLE_ASSISTANT"))) {
//            courses = coman.listOfAllCoursesWithDates(maxPerPage, pageNumber);
//            howManyCoursePage = coman.countAllCoursePages(maxPerPage);
//        } else {
//            courses = coman.listOfMyCoursesWithDates(user, maxPerPage, pageNumber);
//            howManyCoursePage = coman.countMyCoursePages(user, maxPerPage);
//        }
//        
//        DashboardCourseResponse response = cdc.dashboardCourseResponseCreator(courses, howManyCoursePage);
//        return response;
//    }
//
//    @Transactional
//    public DashboardCustomerResponse returnDashboardCustomerData(int maxPerPage, int pageNumber) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String username = auth.getName();
//        User user = ud.findByUsername(username);
//        List<CustomerDto> customers = new ArrayList<>();
//        int howManyClientPages = 0;
//        
//        if (user.getRoles().contains(new Role("ROLE_ADMIN")) || user.getRoles().contains(new Role("ROLE_ASSISTANT"))) {
//            customers = cuman.simpleFindCustomer("", pageNumber, maxPerPage);
//            howManyClientPages = cuman.countCustomerPages(maxPerPage);
//        }
//        
//        DashboardCustomerResponse response = cdc.dashboardCustomerResponseCreator(customers, howManyClientPages);
//        return response;
//        }
//
//}
