package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.classconverter.CustomDtoConverters;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.CourseDto;
import com.manytomany.crmindygo.dto.CourseWithDateDto;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.user.TrainerUser;
import com.manytomany.crmindygo.dto.responses.CourseTypesResponse;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.Location;
import com.manytomany.crmindygo.entities.user.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Márta, Balazs
 */
@Component
public class CourseManipulator {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CourseManipulator cdbman;

    @Autowired
    DtoToEntity dte;

    @Autowired
    CustomDtoConverters cdc;

    /**
     * Save a Course entity in the database.
     *
     * @param course
     */
    @Transactional
    public void saveCourseToDb(Course course) {
        cdbman.addCourseToCourseEvent(course);
        cdbman.addLocationToCourseEvent(course);
        cdbman.addTrainersToCourseEvent(course);
        cdbman.addTypesToCourse(course);
        em.persist(course);
    }

    @Transactional
    public Course modifyCourse(Course course) {
        cdbman.mergeCourseToCourseEvent(course);
        cdbman.addLocationToCourseEvent(course);
        cdbman.mergeTrainersToCourseEvent(course);
        cdbman.mergeTypesToCourse(course);
        em.merge(course);
        return course;
    }

    @Transactional
    public List<CourseTypesResponse> getCourseTypes() {
        List<String> types = em.createQuery("SELECT c.text FROM CourseType c")
                .getResultList();
        List<CourseTypesResponse> typeResponseList = new ArrayList<>();
        for (String type : types) {
            typeResponseList.add(new CourseTypesResponse(type));
        }
        return typeResponseList;

    }

    @Transactional
    public void saveNewType(CourseType courseType) {
        CourseType type = em.find(CourseType.class, courseType.getText());
        if (type == null) {
            em.persist(courseType);
        }
    }

    @Transactional
    public void addCourseToCourseEvent(Course course) {
        List<CourseEvent> courseEventList = course.getCourseEvents();
        for (CourseEvent courseEvent : courseEventList) {
            courseEvent.setRelatedCourse(course);
        }
    }

    @Transactional
    public void addTypesToCourse(Course course) {
        List<CourseType> allTypesFromUser = course.getTypes();
        for (CourseType oneTypeFromUser : allTypesFromUser) {
            CourseType oneTypeFromDb = em.find(CourseType.class, oneTypeFromUser.getText());
            oneTypeFromDb.getCoursesWithThisType().add(course);
        }
    }

    @Transactional
    public void addTrainersToCourseEvent(Course course) {
        List<CourseEvent> courseEventList = course.getCourseEvents();
        for (CourseEvent courseEvent : courseEventList) {
            List<TrainerUser> allTrainersFromUser = courseEvent.getTrainers();
            for (TrainerUser oneTrainerFromUser : allTrainersFromUser) {
                TrainerUser oneTrainerFromDb = em.find(TrainerUser.class, oneTrainerFromUser.getId());
                oneTrainerFromDb.getMyCourseEvents().add(courseEvent);
            }
        }
    }

    @Transactional
    public void mergeTrainersToCourseEvent(Course course) {
        List<CourseEvent> courseEventList = course.getCourseEvents();
        for (CourseEvent courseEvent : courseEventList) {

            CourseEvent courseEventFromDb = em.find(CourseEvent.class, courseEvent.getId());
            if (courseEventFromDb != null) {
                List<TrainerUser> eventTrainersFromDb = courseEventFromDb.getTrainers();

                for (TrainerUser trainerUser : eventTrainersFromDb) {
                    trainerUser.getMyCourseEvents().remove(courseEventFromDb);
                }
            }

            List<TrainerUser> allTrainersFromUser = courseEvent.getTrainers();
            for (TrainerUser oneTrainerFromUser : allTrainersFromUser) {
                TrainerUser oneTrainerFromDb = em.find(TrainerUser.class, oneTrainerFromUser.getId());
                oneTrainerFromDb.getMyCourseEvents().add(courseEvent);
            }
        }
    }

    @Transactional
    public void addLocationToCourseEvent(Course course) {
        List<CourseEvent> courseEventList = course.getCourseEvents();
        for (CourseEvent courseEvent : courseEventList) {
            Address address = returnAddressEntity(courseEvent);
            Location location = returnLocationEntity(courseEvent, address);
            courseEvent.setLocation(location);
        }

    }

    @Transactional
    public Address returnAddressEntity(CourseEvent courseEvent) {
        Address addressFromUser = courseEvent.getLocation().getAddress();
        int zip = addressFromUser.getZip();
        String city = addressFromUser.getCity();
        int number = addressFromUser.getNumber();
        String street = addressFromUser.getStreet();
        try {
            Address addressFromDb = (Address) em.createQuery("SELECT a FROM Address a WHERE a.city = :city AND a.number = :number AND a.street = :street AND a.zip = :zip")
                    .setParameter("city", city)
                    .setParameter("zip", zip)
                    .setParameter("number", number)
                    .setParameter("street", street)
                    .getSingleResult();
            return addressFromDb;
        } catch (NoResultException ex) {
            em.persist(addressFromUser);
            return addressFromUser;
        }
    }

    @Transactional
    public Location returnLocationEntity(CourseEvent courseEvent, Address address) {
        Location locationFromUser = courseEvent.getLocation();
        try {
            Location locationFromDb = (Location) em.createQuery("SELECT l FROM Location l WHERE l.address = :address AND l.room = :room")
                    .setParameter("address", address)
                    .setParameter("room", locationFromUser.getRoom())
                    .getSingleResult();
            return locationFromDb;
        } catch (NoResultException ex) {
            locationFromUser.setAddress(address);
            em.persist(locationFromUser);
            return locationFromUser;
        }
    }

    @Transactional
    public void mergeCourseToCourseEvent(Course course) {
        Course courseFromDb = em.find(Course.class, course.getId());
        List<CourseEvent> courseEventsForThisCourseFromDb = courseFromDb.getCourseEvents();
        List<CourseEvent> allCourseEventsFromUser = course.getCourseEvents();
        for (CourseEvent oneCourseEventFromUser : allCourseEventsFromUser) {
            CourseEvent courseEventFromDb = em.find(CourseEvent.class, oneCourseEventFromUser.getId());
            if (courseEventFromDb != null) {
                courseEventFromDb.setRelatedCourse(null);
                courseEventsForThisCourseFromDb.remove(courseEventFromDb);
            }
            oneCourseEventFromUser.setRelatedCourse(course);
        }

        Iterator<CourseEvent> iterator = courseEventsForThisCourseFromDb.iterator();
        while (iterator.hasNext()) {
            CourseEvent courseEvent = iterator.next();
            List<TrainerUser> trainers = courseEvent.getTrainers();
            for (TrainerUser trainer : trainers) {
                trainer.getMyCourseEvents().remove(courseEvent);
            }
            courseEvent.setRelatedCourse(null);
            courseEvent.setLocation(null);
            em.remove(courseEvent);
        }

//        for (CourseEvent courseEvent : courseEventsForThisCourseFromDb) {
//            List<TrainerUser> trainers = courseEvent.getTrainers();
//            for (TrainerUser trainer : trainers) {
//                trainer.getMyCourseEvents().remove(courseEvent);
//            }
//            courseEvent.setRelatedCourse(null);
//        }
    }

    @Transactional
    public void mergeTypesToCourse(Course course) {
        Course courseFromDb = em.find(Course.class, course.getId());
        List<CourseType> typesOfThisCourseFromDb = courseFromDb.getTypes();
        for (CourseType courseType : typesOfThisCourseFromDb) {
            courseType.getCoursesWithThisType().remove(courseFromDb);
        }

        List<CourseType> allTypesFromUser = course.getTypes();
        for (CourseType oneTypeFromUser : allTypesFromUser) {
            CourseType oneTypeFromDb = em.find(CourseType.class, oneTypeFromUser.getText());
            oneTypeFromDb.getCoursesWithThisType().add(course);
        }

    }

    @Transactional
    public List<CourseWithDateDto> listOfAllCoursesWithDates(int maxResult, int page) {
        int firstResult = ((page - 1) * maxResult);
        List<Course> courseList = em.createQuery("SELECT c FROM Course c ORDER BY c.applicationDeadline DESC")
                .setFirstResult(firstResult)
                .setMaxResults(maxResult)
                .getResultList();
        List<CourseWithDateDto> coursesWithDateList = new ArrayList<>();
        for (Course course : courseList) {
            Date dateStart = (Date) em.createQuery(
                    "SELECT MIN(ce.dateStart) FROM Course c JOIN c.courseEvents ce WHERE c.id = :courseId")
                    .setParameter("courseId", course.getId())
                    .getSingleResult();
            Date dateFinish = (Date) em.createQuery(
                    "SELECT MAX(ce.dateFinish) FROM Course c JOIN c.courseEvents ce WHERE c.id = :courseId")
                    .setParameter("courseId", course.getId())
                    .getSingleResult();
            CourseWithDateDto courseWithDate = cdc.convertCourseToCourseWithDateDto(course, dateStart, dateFinish);
            coursesWithDateList.add(courseWithDate);
        }
        return coursesWithDateList;
    }

    @Transactional
    public List<CourseWithDateDto> listOfMyCoursesWithDates(User user, int maxResult, int page) {
        int firstResult = ((page - 1) * maxResult);
        List<Course> courseList = em.createQuery("SELECT DISTINCT c FROM Course c JOIN c.courseEvents ce JOIN ce.trainers t WHERE t.id = :userId")
                .setParameter("userId", user.getId())
                .setFirstResult(firstResult)
                .setMaxResults(maxResult)
                .getResultList();

        List<CourseWithDateDto> coursesWithDateList = new ArrayList<>();
        for (Course course : courseList) {
            Date dateStart = (Date) em.createQuery(
                    "SELECT MIN(ce.dateStart) FROM Course c JOIN c.courseEvents ce WHERE c.id = :courseId")
                    .setParameter("courseId", course.getId())
                    .getSingleResult();
            Date dateFinish = (Date) em.createQuery(
                    "SELECT MAX(ce.dateFinish) FROM Course c JOIN c.courseEvents ce WHERE c.id = :courseId")
                    .setParameter("courseId", course.getId())
                    .getSingleResult();
            CourseWithDateDto courseWithDate = cdc.convertCourseToCourseWithDateDto(course, dateStart, dateFinish);
            coursesWithDateList.add(courseWithDate);
        }
        return coursesWithDateList;
    }

    public int countAllCoursePages(int resultsPerPage) {
        long numberOfCourses = (long) em.createQuery("SELECT COUNT(c.name) FROM Course c").getSingleResult();
        return (int) Math.ceil(((float) numberOfCourses) / resultsPerPage);
    }

    public int countMyCoursePages(User user, int resultsPerPage) {
        long numberOfMyCourses = (long) em.createQuery("SELECT COUNT(DISTINCT c.id) FROM Course c JOIN c.courseEvents ce JOIN ce.trainers t WHERE t.id = :userId")
                .setParameter("userId", user.getId())
                .getSingleResult();
        return (int) Math.ceil(((double) numberOfMyCourses) / resultsPerPage);
    }
}
