package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.dto.Mail;
import com.manytomany.crmindygo.entities.security.InvitationValidation;
import com.manytomany.crmindygo.dao.InvitationValidationDao;
import com.manytomany.crmindygo.exceptions.IdIsNotValidException;
import com.manytomany.crmindygo.utilites.mailsender.MailService;
import com.manytomany.crmindygo.utilites.mailsender.MailServiceImpl;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sige
 */
@Component
public class InvitationEmailManipulator {
    
    static final Logger LOG = LoggerFactory.getLogger(IdIsNotValidException.class);
    
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    InvitationValidationDao ivd;
    
    @Autowired
    MailService mailservice;
    
    @Transactional
    public void sendInvitation(String role, String name, String email){
        InvitationValidation iv;
        em.persist(iv = new InvitationValidation(role, name, email));
        Mail mail = new Mail();
        mail.setMailFrom("progmaticprojectcinema@gmail.com");
        mail.setMailTo(email);
        mail.setMailSubject("Registration");
        //TODO Állítható!
        //TODO TEMPLATE ÁLLÍTÁS
        String url = "http://crm.project.progmatic.hu/registration/" 
                + iv.getValidationId();
        
        Map< String, Object> model = new HashMap<>();
        model.put("name", name);
        model.put("url", url);
        mail.setModel(model);

        mailservice.sendEmail(mail);
    }
    
    // ALWAYS CATH NULLPOINTEREXCEPTION And THROW AN IDISNOTVALIDEXCEPTION INSTEAD,
    // WHEN YOU USE THIS CODE! TODO FIX THIS.
    public InvitationValidation searchForValidId(String id){
        return ivd.findByValidationId(id);        
    }

    @Transactional
    public void deleteValidationLink(String registrationLinkId) {
        InvitationValidation iv = em.find(InvitationValidation.class, registrationLinkId);
        em.remove(iv);
    }
    
    
    
}
