/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.manipulator;

import com.manytomany.crmindygo.utilities.CourseFindResponseUtility;
import com.manytomany.crmindygo.utilities.DateUtility;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.dto.responses.CourseModifiedResponse;
import com.manytomany.crmindygo.entities.Course;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Márta
 */
@Component
public class SimpleQuiryCourse {
    
    @PersistenceContext
    EntityManager em;

    @Autowired
    DtoToEntity dte;

    @Transactional(readOnly = true)
    public List<CourseFindResponse> findCourse(String searchexpression, int page, int maxResult) {
        int firstResult = ((page - 1) * maxResult);
        Date date = DateUtility.toDate(searchexpression);
        Set<Course> result = new HashSet<>();
        List<Course> courses=new ArrayList<>();
        if (date != null) {
            result.addAll(em.createQuery(
                    "SELECT DISTINCT c FROM Course c left join c.courseEvents ce WHERE "
                    + "ce.dateStart = :searchDate or "
                    + "ce.dateFinish  = :searchDate or "
                    + "c.applicationDeadline  = :searchDate")
                    .setParameter("searchDate", date)
                    .setFirstResult(firstResult-1)
                    .setMaxResults(maxResult)
                    .getResultList());
        } else {
            result.addAll(em.createQuery(
                    "SELECT DISTINCT c FROM Course c left join c.types ct "
                            + "left join c.courseEvents ce left join ce.trainers cet WHERE "
                            + "ct.text like :searchRegExp or "
                            + "ce.location like :searchRegExp or "
                            + "cet.name like :searchRegExp or "
                            + "c.name LIKE :searchRegExp or "
                            + "c.description LIKE :searchRegExp")
                    .setParameter("searchRegExp", "%" + searchexpression + "%")
                    .setFirstResult(firstResult)
                    .setMaxResults(maxResult)
                    .getResultList());
        }
        for (Course course : result) {
            courses.add(course);
        }
        return CourseFindResponseUtility.createCourseFindResponse(courses);
    }

    @Transactional(readOnly = true)
    public CourseModifiedResponse findCourseById(String id) {
        Course course = em.find(Course.class, id);
        return dte.courseBackConverterForSearchById(course);
    }
}
