package com.manytomany.crmindygo.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class DispatcherServlet extends AbstractAnnotationConfigDispatcherServletInitializer{

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {MyServletConfig.class};
    }
    
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {MyRootConfig.class};
    }
    

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
    
}

