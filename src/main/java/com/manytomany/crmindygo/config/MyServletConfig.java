package com.manytomany.crmindygo.config;

import com.manytomany.crmindygo.controller.CustomerRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//TODO Move the JDBC connector to Tomcat/lib from the .war?
@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = CustomerRestController.class)
public class MyServletConfig extends WebMvcConfigurerAdapter {
    
    public static final String[] ALLOWED_CORS_ORIGINS = {"http://localhost:3000", "http://crmdev.indygo-coaching.hu"};
    public static final String CORS_MAPPNIG = "/**";
    public static final String[] ALLOWED_HEADERS = {"*"};
    
    @Autowired
    ApplicationContext context;

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer conf) {
        conf.enable();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(CORS_MAPPNIG)
                .allowedOrigins(ALLOWED_CORS_ORIGINS)
                .allowedMethods("*")
                .allowedHeaders(ALLOWED_HEADERS)
                .allowCredentials(true);
    }
    
    /*
    
    // TODO Review this code. 
    
    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        resolver.setCharacterEncoding("UTF-8");
        resolver.setContentType("text/html;charset=UTF-8");
        return resolver;
    }
    */
}