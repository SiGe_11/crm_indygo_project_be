package com.manytomany.crmindygo.controller;

import com.manytomany.crmindygo.manipulator.CustomerManipulator;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.CustomerDto;
import com.manytomany.crmindygo.entities.Customer;
import com.manytomany.crmindygo.dto.responses.CustomerAddResponse;
import com.manytomany.crmindygo.dto.responses.CustomerCompoundFindResponse;
import com.manytomany.crmindygo.dto.responses.CustomerDeleteResponse;
import java.util.List;
import com.manytomany.crmindygo.dto.responses.CustomerModifyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sige, Tomi
 */
@RestController
public class CustomerRestController {

    @Autowired
    DtoToEntity dte;

    @Autowired
    CustomerManipulator cdbman;

    /*
    Létrehoz egy CustomerAddResponse objektumot és visszaküldi
    {"clientid":1} formátumban
     */
    @RequestMapping(path = "/crm/rest/client", method = RequestMethod.POST)
    public CustomerAddResponse saveClient(@RequestBody CustomerDto customerDto) {
        Customer customer = dte.customerConverter(customerDto);
        cdbman.saveCustomerToDb(customer);
        CustomerAddResponse response = new CustomerAddResponse();
        response.clientId = customer.getId();
        return response;
    }

    /*
    Ügyfélmódosítás befejezése
    kommentelés
    errorcode visszaküldése
     */
    @RequestMapping(path = "/crm/rest/client", method = RequestMethod.PUT)
    public CustomerModifyResponse modifyClient(@RequestBody CustomerDto customerDto) {
        CustomerModifyResponse response = new CustomerModifyResponse();
        try{
            Customer customer = dte.customerConverterWithId(customerDto);
            cdbman.modifyCustomer(customer);
            response.setErrorCode(0);
        } catch (Error e) {
            response.setErrorCode(1);
        }
        return response; 
    }
    
    /*
    Archiválja a customert a database-ben
    visszaküld errorcode-ot (0=jó)
    */
    @RequestMapping(path = "/crm/rest/client", method = RequestMethod.DELETE)
    public CustomerDeleteResponse deleteClient(@RequestBody CustomerDto dto) {
        CustomerDeleteResponse response = new CustomerDeleteResponse();
        try {
            cdbman.deleteCustomer(dto);
            response.setErrorCode(0);
        } catch (Error e) {
            response.setErrorCode(1);
        }
        return response;
    }
    
    /**
     * Rest side for search for customer in all their fields. 
     * @param searchexpression
     * @param page
     * @param size
     * @return List<CustomerDto>
     */
    @RequestMapping(value = "/crm/rest/client", method = RequestMethod.GET)
    public CustomerCompoundFindResponse findCustomer(
                @RequestParam(name = "searchexpression", defaultValue = "") String searchexpression, 
                @RequestParam(name = "name", defaultValue = "*") String name, 
                @RequestParam(name ="email", defaultValue = "*") String email,
                @RequestParam(name = "status", defaultValue = "*") String status, 
                @RequestParam(name ="pagenumber", defaultValue = "1") int pagenumber, 
                @RequestParam(name = "maxperpage", defaultValue = "10") int maxperpage) { 
        if (searchexpression.isEmpty()) {
                   return cdbman.compoundFindCustomer(name, email, status, pagenumber, maxperpage); 
                } 
        else return cdbman.simpleFindCustomer(searchexpression, pagenumber, maxperpage);
    }
    
    //TODO HANDLE NULL POINTER
    @RequestMapping(value = "/crm/rest/clientby", method = RequestMethod.GET)
    public CustomerDto findCustomerByID(@RequestParam("id") String id) {
        return cdbman.findCustomerById(id);
    }

    /**
     * Rest side for the listing of all customers.
     * @return List 
     */
    @RequestMapping(value = "/crm/rest/client/all", method = RequestMethod.GET)
    public List<CustomerDto> listOfAllCustomer() {
        return cdbman.listOfAllCustomers();
    }
    
    @RequestMapping(value = "/crm/rest/client/alltocsv", method = RequestMethod.GET)
    public void writeListOfAllCustomerToCsv() {
        cdbman.allCustomersToCsv();
    }
   
    
}
