package com.manytomany.crmindygo.controller;

import com.manytomany.crmindygo.classconverter.CustomDtoConverters;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.RegistrationDto;
import com.manytomany.crmindygo.dto.responses.RegistrationFailedResponse;
import com.manytomany.crmindygo.dto.responses.RegistrationOkResponse;
import com.manytomany.crmindygo.exceptions.UserAlreadyExists;
import com.manytomany.crmindygo.manipulator.UserManipulator;
import org.springframework.web.bind.annotation.PostMapping;
import com.manytomany.crmindygo.dto.InvitationDto;
import com.manytomany.crmindygo.dto.responses.IdIsNotValidResponse;
import com.manytomany.crmindygo.dto.responses.NameEmailRoleResponse;
import com.manytomany.crmindygo.dto.responses.TrainersResponse;
import com.manytomany.crmindygo.entities.security.InvitationValidation;
import com.manytomany.crmindygo.exceptions.IdIsNotValidException;
import com.manytomany.crmindygo.exceptions.InvalidRegistrationAttempt;
import com.manytomany.crmindygo.manipulator.InvitationEmailManipulator;
import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author sige, Balazs
 */
@RestController
public class UserRestController {

    @Autowired
    UserManipulator userManipulator;

    @Autowired
    InvitationEmailManipulator iem;

    @Autowired
    CustomDtoConverters cdc;
    
    @Autowired
    DtoToEntity dte;

    @PostMapping(path = "/crm/rest/user")
    public RegistrationOkResponse registerUser(@RequestBody RegistrationDto registrationDto) {
        try {
            String validationId = registrationDto.getGeneratedId();
            InvitationValidation invitationValidation = iem.searchForValidId(validationId); //throws Idisnotvalid Exception
            String roleFromDb = invitationValidation.getUserRole();
            String userId = userManipulator.saveUserToDb(registrationDto, roleFromDb);
            return new RegistrationOkResponse(userId);
        } catch (NullPointerException npe) {
            throw new IdIsNotValidException();
        }
    }

    /**
     * Return with informations of the user who is actually logged in.
     *
     * @param principal
     * @return json
     */
    //TODO Adatbázisból kikeresni és visszaadni jsonként.
    @RequestMapping(path = "/crm/rest/users/current", method = RequestMethod.GET)
    @ResponseBody
    public NameEmailRoleResponse currentUserName(Principal principal) {
        return cdc.currentUserCreator(userManipulator.currentUser(principal.getName()));
    }

    /**
     * Creates a registration link.
     *
     * @param InvitationDto
     * @return json
     */
    @RequestMapping(path = "/crm/rest/invitation", method = RequestMethod.POST)
    public String invitation(@RequestBody InvitationDto idto) {
        iem.sendInvitation(idto.getRole(), idto.getName(), idto.getEmail());
        return "\n{\n	\"status\": \"ok\"\n}";
    }

    @RequestMapping(path = "/crm/rest/register", method = RequestMethod.GET)
    public NameEmailRoleResponse register (@RequestParam("id") String id){
        try{
            InvitationValidation iv = iem.searchForValidId(id);
            return  cdc.toRegisterUserCreator(iv.getName(), iv.getEmail(), iv.getUserRole());
        } catch (NullPointerException npe) {
            throw new IdIsNotValidException();
            //"e caddi come corpo morto cade"
      }        
    }

    @GetMapping(path = "/crm/rest/trainers")
    public List<TrainersResponse> listTrainerNames() {
        List<TrainersResponse> trainerNames = userManipulator.getTrainers();
        return trainerNames;
    }

    @ExceptionHandler(UserAlreadyExists.class)
    public RegistrationFailedResponse handleUserAlreadyExistsError(UserAlreadyExists ex) {
        return new RegistrationFailedResponse(ex.getMessage());
    }

    @ExceptionHandler(IdIsNotValidException.class)
    public IdIsNotValidResponse idIsNotValidError(IdIsNotValidException ex) {
        return new IdIsNotValidResponse();
    }

    @ExceptionHandler(InvalidRegistrationAttempt.class)
    public RegistrationFailedResponse handleInvalidRegistrationAttemptError(InvalidRegistrationAttempt ex) {
        return new RegistrationFailedResponse(ex.getMessage());
    }
}
