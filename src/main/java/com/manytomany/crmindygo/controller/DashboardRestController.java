//package com.manytomany.crmindygo.controller;
//
//import com.manytomany.crmindygo.dto.responses.DashboardCourseResponse;
//import com.manytomany.crmindygo.dto.responses.DashboardCustomerResponse;
//import com.manytomany.crmindygo.dto.responses.DashboardResponse;
//import com.manytomany.crmindygo.manipulator.DashboardManipulator;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// *
// * @author Balazs
// */
//@RestController
//public class DashboardRestController {
//
//    @Autowired
//    DashboardManipulator dsman;
//
//    @GetMapping(path = "/crm/rest/dashboard")
//    public DashboardResponse returnDashboardData() {
//        return dsman.returnDashboardData();
//    }
//
//    @GetMapping(value = "/crm/rest/dashboard/courses")
//    public DashboardCourseResponse returnDashboardCourseData(
//            @RequestParam(name = "pagenumber", defaultValue = "1") int pageNumber,
//            @RequestParam(name = "maxperpage", defaultValue = "5") int maxPerPage) {
//        return dsman.returnDashboardCourseData(maxPerPage, pageNumber);
//    }
//
//    @GetMapping(value = "/crm/rest/dashboard/clients")
//    public DashboardCustomerResponse returnDashboardCustomerData(
//            @RequestParam(name = "pagenumber", defaultValue = "1") int pageNumber,
//            @RequestParam(name = "maxperpage", defaultValue = "5") int maxPerPage) {
//        return dsman.returnDashboardCustomerData(maxPerPage, pageNumber);
//    }
//
//}
