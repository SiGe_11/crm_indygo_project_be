/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.controller;

import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.manipulator.CourseManipulator;
import com.manytomany.crmindygo.dto.CourseDto;
import com.manytomany.crmindygo.dto.CourseTypeDto;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.exceptions.SavingCourseToDatabaseFailed;
import com.manytomany.crmindygo.dto.responses.CourseAddResponse;
import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.dto.responses.CourseModifiedResponse;
import com.manytomany.crmindygo.dto.responses.CourseTypesResponse;
import com.manytomany.crmindygo.manipulator.CompoundQuiryCourse;
import com.manytomany.crmindygo.manipulator.SimpleQuiryCourse;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Márta ,Balázs
 */
@RestController
public class CourseRestController {

    static final Logger LOG = LoggerFactory.getLogger(CourseRestController.class);

    @Autowired
    DtoToEntity dte;

    @Autowired
    CourseManipulator cdbman;

    @PersistenceContext
    EntityManager em;

    @Autowired
    SimpleQuiryCourse simpleFindCourse;
    
    @Autowired
    CompoundQuiryCourse compoundSearchCourse;

    @GetMapping(path = "/crm/rest/courses/types")
    public List<CourseTypesResponse> listCourseTypes() {
        List<CourseTypesResponse> courseTypes = cdbman.getCourseTypes();
        return courseTypes;
    }

    @PostMapping(path = "/crm/rest/courses/types")
    public void postNewCourseType(@RequestBody List<CourseTypeDto> courseTypeDtoList) {
        CourseTypeDto courseTypeDto = courseTypeDtoList.get(0); //így lett definiálva a REST interface, pedig akár a List elhagyható lenne
        CourseType courseType = dte.courseTypeConverter(courseTypeDto);
        cdbman.saveNewType(courseType);
    }

    @PostMapping(path = "/crm/rest/courses")
    public CourseAddResponse saveCourse(@RequestBody CourseDto courseDto) {
        Course course = dte.courseConverter(courseDto);
        try {
            cdbman.saveCourseToDb(course);
        } catch (Exception ex) {
            throw new SavingCourseToDatabaseFailed("savingToDatabaseFailed");
        }
        CourseAddResponse response = new CourseAddResponse();
        response.courseId = course.getId();
        return response;
    }

    @RequestMapping(path = "/crm/rest/courses", method = RequestMethod.PUT)
    public String modifyCourse(@RequestBody CourseDto courseDto) {
        Course course = dte.courseConverterWithId(courseDto);
        try {
            course = cdbman.modifyCourse(course);
        } catch (Exception ex) {
            throw new SavingCourseToDatabaseFailed("savingToDatabaseFailed");
        }
        return " \n{\n	\"status\": \"ok\"\n}";
    }

    @RequestMapping(value = "/crm/rest/courses", method = RequestMethod.GET)
    public List<CourseFindResponse> findCourse(
            @RequestParam(defaultValue = "", name = "searchexpression") String searchexpression,
            @RequestParam(defaultValue = "", name = "type" ) String type, 
            @RequestParam(defaultValue = "", name = "name") String name, 
            @RequestParam(defaultValue = "", name = "dateStartBefore") String dateStartBefore, 
            @RequestParam(defaultValue = "", name = "dateStartAfter") String dateStartAfter, 
            @RequestParam(defaultValue = "", name = "dateFinishBefore") String dateFinishBefore, 
            @RequestParam(defaultValue = "", name = "dateFinishAfter") String dateFinishAfter, 
            @RequestParam(defaultValue = "", name = "location") String location,
            @RequestParam(name = "pagenumber", defaultValue = "1") int page,
            @RequestParam(name = "maxperpage", defaultValue = "10") int size){
        if(searchexpression.isEmpty())return compoundSearchCourse.findCourse(type, 
                name, dateStartBefore, dateStartAfter, dateFinishBefore, dateFinishAfter, 
                location, page, size);
        else return simpleFindCourse.findCourse(searchexpression, page, size);
    }

    @RequestMapping(value = "/crm/rest/coursesbyid", method = RequestMethod.GET)
    public CourseModifiedResponse findCourseById(@RequestParam("courseid") String id) {
        return simpleFindCourse.findCourseById(id);
    }

    @ExceptionHandler(SavingCourseToDatabaseFailed.class)
    public SavingCourseToDatabaseFailed handleRestErrors(SavingCourseToDatabaseFailed ex) {
        return new SavingCourseToDatabaseFailed(ex.getMessage());
    }

}
