/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.controller;

import com.manytomany.crmindygo.dto.responses.AddressesResponse;
import com.manytomany.crmindygo.dto.responses.StatusResponse;
import com.manytomany.crmindygo.manipulator.EnumManipulator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Balazs
 */
@RestController
public class EnumRestController {
    
    @Autowired
    EnumManipulator enman;
    
    @GetMapping(path = "/crm/rest/enum/status")
    public List<StatusResponse> returnStatuses() {
        return enman.returnStatuses();
    }
    
    @GetMapping(path = "/crm/rest/enum/addresses")
    public AddressesResponse returnAddresses() {
        return enman.returnAddresses();
    }
    
}
