/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities.user;

import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.CourseEvent;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

/**
 *
 * @author Balazs
 */
@Entity
public class TrainerUser extends User {
    
    @ManyToMany
    List<Address> notAcceptedLocations;
    
    @ManyToMany
    List<CourseEvent> myCourseEvents;

    public TrainerUser() {
    }

    public TrainerUser(List<Address> nonAcceptedLocations, List<CourseEvent> myCourseEvents, String name, String gender, String telephone, String email, Address address, String username, String password, boolean enabled, List<Role> roles) {
        super(name, gender, telephone, email, address, username, password, enabled, roles);
        this.notAcceptedLocations = nonAcceptedLocations;
        this.myCourseEvents = myCourseEvents;
    }

    public List<Address> getNonAcceptedLocations() {
        return notAcceptedLocations;
    }

    public void setNonAcceptedLocations(List<Address> nonAcceptedLocations) {
        this.notAcceptedLocations = nonAcceptedLocations;
    }

    public List<CourseEvent> getMyCourseEvents() {
        return myCourseEvents;
    }

    public void setMyCourseEvents(List<CourseEvent> myCourseEvents) {
        this.myCourseEvents = myCourseEvents;
    }

    @Override
    public String toString() {
        return this.getId();
    }

    
}
