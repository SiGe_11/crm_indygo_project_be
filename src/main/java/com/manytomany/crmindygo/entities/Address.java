/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities;

import com.manytomany.crmindygo.entities.user.TrainerUser;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Balazs
 */
@Entity
public class Address extends EntityWithId {
    
    private int zip;
    private String city;
    private String street;
    private int number;
    private String other;
    
    @OneToMany(mappedBy = "address")
    private List<Location> locationsWithThisAddress;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "notAcceptedLocations")
    private List<TrainerUser> trainersNotAcceptingThisAddress;
    
    public Address() {
    }

    public Address(int zip, String city, String street, int number, String other) {
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
        this.other = other;
    }

    
    
    public Address(int zip, String city, String street, int number, String other, List<Location> locationsWithThisAddress, List<TrainerUser> trainersNotAcceptingThisAddress) {
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
        this.other = other;
        this.locationsWithThisAddress = locationsWithThisAddress;
        this.trainersNotAcceptingThisAddress = trainersNotAcceptingThisAddress;
    }

    
    
    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    
    public List<Location> getLocationsWithThisAddress() {
        return locationsWithThisAddress;
    }

    public void setLocationsWithThisAddress(List<Location> locationsWithThisAddress) {
        this.locationsWithThisAddress = locationsWithThisAddress;
    }

    public List<TrainerUser> getTrainersNotAcceptingThisAddress() {
        return trainersNotAcceptingThisAddress;
    }

    public void setTrainersNotAcceptingThisAddress(List<TrainerUser> trainersNotAcceptingThisAddress) {
        this.trainersNotAcceptingThisAddress = trainersNotAcceptingThisAddress;
    }
    
}
