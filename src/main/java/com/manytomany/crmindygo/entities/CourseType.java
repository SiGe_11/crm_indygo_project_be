/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author Balazs
 */
@Entity
public class CourseType implements Serializable {
    
    @Id
    String text;
    
    @ManyToMany
    List<Course> coursesWithThisType;

    public CourseType() {
    }

    public CourseType(String text, List<Course> coursesWithThisType) {
        this.text = text;
        this.coursesWithThisType = coursesWithThisType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Course> getCoursesWithThisType() {
        return coursesWithThisType;
    }

    public void setCoursesWithThisType(List<Course> coursesWithThisType) {
        this.coursesWithThisType = coursesWithThisType;
    }

    @Override
    public String toString() {
        return this.text;
    }

    

    
}
