package com.manytomany.crmindygo.entities;

import com.manytomany.crmindygo.dto.*;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tomi, Balázs
 */
@Entity
public class Customer extends EntityWithId {

    private String name;
    private String email;
    private String gender;
    private String phone;
    private String education;
    private String birthPlace;
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDateContact;
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    private String status;
    private String zip;
    private String city;
    private String street;
    private String number;
    private String other;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Course> courses;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Course> prevcourses;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Course> intrestedcourses;
    private String otherInformation;
    private String job;
    
   
    //TODO kapcsolódni a kurzusokhoz, akár explicit enititással is ha el akarjuk tárolni a pl hogy mikor fizetett vagy ilyesmi (?)
 
    public Customer() {
    }

    public Customer(String name, String email, String gender, String phone, String education, String birthPlace, Date startDateContact, Date birthDate, String status, String zip, String city, String street, String number, String other, List<Course> courses, List<Course> prevcourses, List<Course> intrestedcourses, String otherInformation, String job) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.phone = phone;
        this.education = education;
        this.birthPlace = birthPlace;
        this.startDateContact = startDateContact;
        this.birthDate = birthDate;
        this.status = status;
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
        this.other = other;
        this.courses = courses;
        this.prevcourses = prevcourses;
        this.intrestedcourses = intrestedcourses;
        this.otherInformation = otherInformation;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Date getStartDateContact() {
        return startDateContact;
    }

    public void setStartDateContact(Date startDateContact) {
        this.startDateContact = startDateContact;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getPrevcourses() {
        return prevcourses;
    }

    public void setPrevcourses(List<Course> prevcourses) {
        this.prevcourses = prevcourses;
    }

    public List<Course> getIntrestedcourses() {
        return intrestedcourses;
    }

    public void setIntrestedcourses(List<Course> intrestedcourses) {
        this.intrestedcourses = intrestedcourses;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    

}
