/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities;

import com.manytomany.crmindygo.entities.user.TrainerUser;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Balazs
 */
@Entity
public class CourseEvent extends EntityWithId {
    
    private String name;
    private String courseEventDescription;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateStart;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFinish;
    
    @ManyToOne
    private Location location;
    
    @ManyToMany(mappedBy = "myCourseEvents")
    private List<TrainerUser> trainers;

    @ManyToOne
    private Course relatedCourse;

    public CourseEvent() {
    }

    public CourseEvent(String name, String courseEventDescription, Date dateStart, Date dateFinish, Location location, List<TrainerUser> trainers, Course relatedCourse) {
        this.name = name;
        this.courseEventDescription = courseEventDescription;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.location = location;
        this.trainers = trainers;
        this.relatedCourse = relatedCourse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseEventDescription() {
        return courseEventDescription;
    }

    public void setCourseEventDescription(String courseEventDescription) {
        this.courseEventDescription = courseEventDescription;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<TrainerUser> getTrainers() {
        return trainers;
    }

    public void setTrainers(List<TrainerUser> trainers) {
        this.trainers = trainers;
    }

    public Course getRelatedCourse() {
        return relatedCourse;
    }

    public void setRelatedCourse(Course relatedCourse) {
        this.relatedCourse = relatedCourse;
    }

}
