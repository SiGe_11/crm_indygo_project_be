package com.manytomany.crmindygo.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Balazs
 */
@Entity
public class Status {
    
    @Id
    String status;

    public Status() {
    }

    public Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
