package com.manytomany.crmindygo.entities.security;

import static com.manytomany.crmindygo.utilites.security.SecureIdGenerator.secureId;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Balázs, sige
 */

@Component
@Entity
public class InvitationValidation implements Serializable {

    @Id
    private String validationId;
    private String userRole;
    private String name;
    private String email;
    
    static final Logger LOG = LoggerFactory.getLogger(InvitationValidation.class);

    public InvitationValidation() {
    }
   
    public InvitationValidation(String role, String name, String email) {
       
        try {
            this.validationId = secureId();
            this.userRole=role;
            this.name = name;
            this.email = email;
        } catch (NoSuchAlgorithmException ex) {
            LOG.debug("No such algorithm exception.");
        }          
    }

    public String getValidationId() {
        return validationId;
    }

    public void setValidationId(String validationId) {
        this.validationId = validationId;
    }  

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
 
    
    
}
