/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Márta, Balázs
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "courseWithCourseEvents",
            attributeNodes = {
                @NamedAttributeNode(value = "id"),
                @NamedAttributeNode(value = "courseEvents")
            })
})
public class Course extends EntityWithId {
    
    @ManyToMany(mappedBy = "coursesWithThisType")
    private List<CourseType> types;
    private String name;
    private String description;
    
    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "relatedCourse")
    private List<CourseEvent> courseEvents;
    private String discount;
    private int maxStudents;
    private int minStudents;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date applicationDeadline;
    private int price;
    
    public Course() {
    }

    public Course(List<CourseType> types, String name, String description, List<CourseEvent> courseEvents, String discount, int maxStudents, int minStudents, Date applicationDeadline, int price) {
        this.types = types;
        this.name = name;
        this.description = description;
        this.courseEvents = courseEvents;
        this.discount = discount;
        this.maxStudents = maxStudents;
        this.minStudents = minStudents;
        this.applicationDeadline = applicationDeadline;
        this.price = price;
    }

    
    public List<CourseType> getTypes() {
        return types;
    }

    public void setTypes(List<CourseType> types) {
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CourseEvent> getCourseEvents() {
        return courseEvents;
    }

    public void setCourseEvents(List<CourseEvent> courseEvents) {
        this.courseEvents = courseEvents;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public void setMinStudents(int minStudents) {
        this.minStudents = minStudents;
    }

    public Date getApplicationDeadline() {
        return applicationDeadline;
    }

    public void setApplicationDeadline(Date applicationDeadline) {
        this.applicationDeadline = applicationDeadline;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

}
