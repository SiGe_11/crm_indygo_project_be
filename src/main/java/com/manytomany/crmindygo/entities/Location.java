/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.entities;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Balazs
 */
@Entity
public class Location extends EntityWithId {
    
    @ManyToOne
    private Address address;
    private String room;
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "location")
    List<CourseEvent> courseEventsAtThisLocation;

    public Location() {
    }

    public Location(Address address, String room, List<CourseEvent> courseEventsAtThisLocation) {
        this.address = address;
        this.room = room;
        this.courseEventsAtThisLocation = courseEventsAtThisLocation;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public List<CourseEvent> getCourseEventsAtThisLocation() {
        return courseEventsAtThisLocation;
    }

    public void setCourseEventsAtThisLocation(List<CourseEvent> courseEventsAtThisLocation) {
        this.courseEventsAtThisLocation = courseEventsAtThisLocation;
    }
    
    
}
