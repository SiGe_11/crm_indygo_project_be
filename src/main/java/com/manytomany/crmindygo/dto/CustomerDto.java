/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manytomany.crmindygo.dto;

import com.manytomany.crmindygo.entities.Address;
import java.util.Date;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Tomi, Balázs
 */

public class CustomerDto {
    
    private String id;
    private String name;
    private String email;
    private String gender;
    private String phone;
    private String education;
    private String birthPlace;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDateContact;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private String status;
    private String zip;
    private String city;
    private String street;
    private String number;
    private String other;
    private List<String> courses;
    private List<String> prevcourses;
    private List<String> intrestedcourses;
    private String otherInformation;
    private String job;

    public CustomerDto() {
    }

    public CustomerDto(String name, String email, String gender, String phone, String education, String birthPlace, Date startDateContact, Date birthDate, String status, String zip, String city, String street, String number, String other, List<String> courses, List<String> prevcourses, List<String> intrestedcourses, String otherInformation, String job) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.phone = phone;
        this.education = education;
        this.birthPlace = birthPlace;
        this.startDateContact = startDateContact;
        this.birthDate = birthDate;
        this.status = status;
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
        this.other = other;
        this.courses = courses;
        this.prevcourses = prevcourses;
        this.intrestedcourses = intrestedcourses;
        this.otherInformation = otherInformation;
        this.job = job;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Date getStartDateContact() {
        return startDateContact;
    }

    public void setStartDateContact(Date startDateContact) {
        this.startDateContact = startDateContact;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public List<String> getPrevcourses() {
        return prevcourses;
    }

    public void setPrevcourses(List<String> prevcourses) {
        this.prevcourses = prevcourses;
    }

    public List<String> getIntrestedcourses() {
        return intrestedcourses;
    }

    public void setIntrestedcourses(List<String> intrestedcourses) {
        this.intrestedcourses = intrestedcourses;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }


    

}
