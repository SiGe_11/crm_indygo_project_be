package com.manytomany.crmindygo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class CourseWithDateDto {
    
    private List<String> type;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date dateStart;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date dateFinish;
    private String location; //ez csak a város és irányítószám lesz itt!
    private String id;
    private String actClients;
    private String minClients;
    private String inquirer;

    public CourseWithDateDto() {
    }

    public CourseWithDateDto(List<String> type, String name, Date dateStart, Date dateFinish, String location, String id, String actClients, String minClients, String inquirer) {
        this.type = type;
        this.name = name;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.location = location;
        this.id = id;
        this.actClients = actClients;
        this.minClients = minClients;
        this.inquirer = inquirer;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActClients() {
        return actClients;
    }

    public void setActClients(String actClients) {
        this.actClients = actClients;
    }

    public String getMinClients() {
        return minClients;
    }

    public void setMinClients(String minClients) {
        this.minClients = minClients;
    }

    public String getInquirer() {
        return inquirer;
    }

    public void setInquirer(String inquirer) {
        this.inquirer = inquirer;
    }
    
}

