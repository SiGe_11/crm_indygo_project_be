package com.manytomany.crmindygo.dto;

/**
 *
 * @author sige
 */
public class InvitationDto {

    String name;
    String email;
    String role;
    String text;
    int expiry;

    public InvitationDto() {
    }

    public InvitationDto(String name, String email, String role, String text, int expiry) {
        this.name = name;
        this.email = email;
        this.role = role;
        this.text = text;
        this.expiry = expiry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getExpiry() {
        return expiry;
    }

    public void setExpiry(int expiry) {
        this.expiry = expiry;
    }
    
    
    
}
