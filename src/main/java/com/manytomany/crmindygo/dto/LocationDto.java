/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

/**
 *
 * @author Balazs
 */
public class LocationDto {
    
    private AddressDto address;
    private String room;

    public LocationDto() {
    }

    public LocationDto(AddressDto address, String room) {
        this.address = address;
        this.room = room;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    }
