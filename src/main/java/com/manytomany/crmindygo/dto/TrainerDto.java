/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

/**
 *
 * @author Balazs
 */
public class TrainerDto {
    
    String id;

    public TrainerDto() {
    }

    public TrainerDto(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
