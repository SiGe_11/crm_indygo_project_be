/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

/**
 *
 * @author Balazs
 */
public class CourseTypeDto {
    
    String text;

    public CourseTypeDto() {
    }

    public CourseTypeDto(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
    
}
