package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author TANULO
 */
public class CustomerModifyResponse {

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
    
    public int errorCode = 1;
        
}
