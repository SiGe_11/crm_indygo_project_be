package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Balazs
 */
public class RegistrationFailedResponse {
    
    public String errorCause;

    public RegistrationFailedResponse(String errorCause) {
        this.errorCause = errorCause;
    }
    
    
}
