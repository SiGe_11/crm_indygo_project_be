package com.manytomany.crmindygo.dto.responses;

import com.manytomany.crmindygo.dto.CourseWithDateDto;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class DashboardCourseResponse {
    
    List<CourseWithDateDto> courses;
    int howManyCoursesPage;

    public DashboardCourseResponse() {
    }

    public DashboardCourseResponse(List<CourseWithDateDto> courses, int howManyCoursesPage) {
        this.courses = courses;
        this.howManyCoursesPage = howManyCoursesPage;
    }

    public List<CourseWithDateDto> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseWithDateDto> courses) {
        this.courses = courses;
    }

    public int getHowManyCoursesPage() {
        return howManyCoursesPage;
    }

    public void setHowManyCoursesPage(int howManyCoursesPage) {
        this.howManyCoursesPage = howManyCoursesPage;
    }
    
    
}
