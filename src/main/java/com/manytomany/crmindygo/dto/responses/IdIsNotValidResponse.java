package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author sige
 */
public class IdIsNotValidResponse {

    public final String errorCode = "NotValidRegUrl";
    
    public IdIsNotValidResponse() {
    }
    
}
