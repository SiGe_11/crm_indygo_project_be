package com.manytomany.crmindygo.dto.responses;

import com.manytomany.crmindygo.dto.AddressDto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class AddressesResponse {
    
    List<AddressDto> addresses = new ArrayList<>();

    public AddressesResponse() {
    }

    public AddressesResponse(List<AddressDto> addresses) {
        this.addresses = addresses;
    }

    public List<AddressDto> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressDto> addresses) {
        this.addresses = addresses;
    }
    
    
}
