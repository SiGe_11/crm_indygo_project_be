/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manytomany.crmindygo.dto.responses;


import java.util.List;

/**
 *
 * @author sige
 */
public class CustomerCompoundFindResponse {

    List<NameIdEmailStatus> users; //NOTE hibás interface miatt hívjuk itt usersnek, helyesen customers lenne.
    String pageCount;

    public CustomerCompoundFindResponse() {
    }

    public CustomerCompoundFindResponse(List<NameIdEmailStatus> users, int pageCount) {
        this.users = users;
        this.pageCount = String.valueOf(pageCount);
    }

    public List<NameIdEmailStatus> getUsers() {
        return users;
    }

    public void setUsers(List<NameIdEmailStatus> users) {
        this.users = users;
    }

    public String getPageCount() {
        return pageCount;
    }

    public void setPageCount(String pageCount) {
        this.pageCount = pageCount;
    }

    
    
}
