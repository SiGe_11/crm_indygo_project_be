package com.manytomany.crmindygo.dto.responses;

import com.manytomany.crmindygo.dto.CourseWithDateDto;
import com.manytomany.crmindygo.dto.CustomerDto;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class DashboardResponse {

    List<CustomerDashboardResponse> clients;
    int howManyClientPages;
    List<CourseWithDateDto> courses;
    int howManyCoursesPage;

    public DashboardResponse(List<CustomerDashboardResponse> clients, int howManyClientPages, List<CourseWithDateDto> courses, int howManyCoursesPage) {
        this.clients = clients;
        this.howManyClientPages = howManyClientPages;
        this.courses = courses;
        this.howManyCoursesPage = howManyCoursesPage;
    }

    public List<CustomerDashboardResponse> getClients() {
        return clients;
    }

    public void setClients(List<CustomerDashboardResponse> clients) {
        this.clients = clients;
    }

    public int getHowManyClientPages() {
        return howManyClientPages;
    }

    public void setHowManyClientPages(int howManyClientPages) {
        this.howManyClientPages = howManyClientPages;
    }

    public List<CourseWithDateDto> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseWithDateDto> courses) {
        this.courses = courses;
    }

    public int getHowManyCoursesPage() {
        return howManyCoursesPage;
    }

    public void setHowManyCoursesPage(int howManyCoursesPage) {
        this.howManyCoursesPage = howManyCoursesPage;
    }
    
    
}
