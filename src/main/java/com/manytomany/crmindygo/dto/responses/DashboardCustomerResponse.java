package com.manytomany.crmindygo.dto.responses;

import java.util.List;

/**
 *
 * @author Balazs
 */
public class DashboardCustomerResponse {
    
    List<CustomerDashboardResponse> clients;
    int howManyClientPages;

    public DashboardCustomerResponse() {
    }

    public DashboardCustomerResponse(List<CustomerDashboardResponse> clients, int howManyClientPages) {
        this.clients = clients;
        this.howManyClientPages = howManyClientPages;
    }

    public List<CustomerDashboardResponse> getClients() {
        return clients;
    }

    public void setClients(List<CustomerDashboardResponse> clients) {
        this.clients = clients;
    }

    public int getHowManyClientPages() {
        return howManyClientPages;
    }

    public void setHowManyClientPages(int howManyClientPages) {
        this.howManyClientPages = howManyClientPages;
    }
    
}
