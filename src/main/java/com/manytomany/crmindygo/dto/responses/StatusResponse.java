package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Balazs
 */
public class StatusResponse {
    
    String status;

    public StatusResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
