package com.manytomany.crmindygo.dto.responses;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sige
 */
public class NameEmailRoleResponse {

    private String name;
    private String email;
    private List<String> role;

    public NameEmailRoleResponse(String name, String email, List<String> role) {
        this.name = name;
        this.email = email;
        this.role = role;
    }
    
    public NameEmailRoleResponse(String name, String email, String role) {
        this.name = name;
        this.email = email;
        List<String> roles = new ArrayList<>();
        roles.add(role);
        this.role = roles;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }
    
}
