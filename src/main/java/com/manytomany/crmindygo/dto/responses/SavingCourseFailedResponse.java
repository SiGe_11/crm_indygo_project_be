/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Balazs
 */
public class SavingCourseFailedResponse {
    
    public String errorCause;

    public SavingCourseFailedResponse(String errorCause) {
        this.errorCause = errorCause;
    }
}
