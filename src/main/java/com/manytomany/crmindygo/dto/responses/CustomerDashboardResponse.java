package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Balazs
 */
public class CustomerDashboardResponse {
    
    String name;
    String id;
    String email;
    String status;

    public CustomerDashboardResponse(String name, String id, String email, String status) {
        this.name = name;
        this.id = id;
        this.email = email;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
