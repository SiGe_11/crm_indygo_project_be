/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Márta
 */
public class CourseFindResponse {
    
    public String[] type;
    public String name;
    public String dateStart;
    public String dateFinish;
    public String location;
    public String id;
    
}
