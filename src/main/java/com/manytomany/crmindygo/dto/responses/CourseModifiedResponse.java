package com.manytomany.crmindygo.dto.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manytomany.crmindygo.dto.CourseEventDto;
import com.manytomany.crmindygo.dto.CourseTypeDto;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class CourseModifiedResponse {
    
    private String id;
    private List<String> types; //A frontend kérésére nem CourseType objektumok, hanem egy String lista kerül átadásra.
    private String name;
    private String description;
    private List<CourseEventModifiedResponse> courseEvents; //A frontend kérésére nem Trainer objektumok, hanem egy String lista kerül átadásra.
    private String discount;
    private int maxStudents;
    private int minStudents;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date applicationDeadline;
    private int price;

    public CourseModifiedResponse() {
    }

    public CourseModifiedResponse(String id, List<String> types, String name, String description, List<CourseEventModifiedResponse> courseEvents, String discount, int maxStudents, int minStudents, Date applicationDeadline, int price) {
        this.id = id;
        this.types = types;
        this.name = name;
        this.description = description;
        this.courseEvents = courseEvents;
        this.discount = discount;
        this.maxStudents = maxStudents;
        this.minStudents = minStudents;
        this.applicationDeadline = applicationDeadline;
        this.price = price;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CourseEventModifiedResponse> getCourseEvents() {
        return courseEvents;
    }

    public void setCourseEvents(List<CourseEventModifiedResponse> courseEvents) {
        this.courseEvents = courseEvents;
    }


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public void setMinStudents(int minStudents) {
        this.minStudents = minStudents;
    }

    public Date getApplicationDeadline() {
        return applicationDeadline;
    }

    public void setApplicationDeadline(Date applicationDeadline) {
        this.applicationDeadline = applicationDeadline;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
}
