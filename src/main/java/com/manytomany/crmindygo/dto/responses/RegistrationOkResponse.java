/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto.responses;

/**
 *
 * @author Balazs
 */
public class RegistrationOkResponse {
    
    public String userID;

    public RegistrationOkResponse(String userID) {
        this.userID = userID;
    }
    
}
