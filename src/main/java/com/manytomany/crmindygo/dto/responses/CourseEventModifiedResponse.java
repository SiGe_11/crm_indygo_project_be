package com.manytomany.crmindygo.dto.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manytomany.crmindygo.dto.LocationDto;
import com.manytomany.crmindygo.dto.TrainerDto;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class CourseEventModifiedResponse {
    
    private String id;
    private String name;
    private String courseEventDescription;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date dateStart;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date dateFinish;
    
    private LocationDto location; 
    
    private List<String> trainers; //A frontend kérésére nem Trainer objektumok, hanem egy String lista kerül átadásra.

    public CourseEventModifiedResponse() {
    }

    public CourseEventModifiedResponse(String id, String name, String courseEventDescription, Date dateStart, Date dateFinish, LocationDto location, List<String> trainers) {
        this.id = id;
        this.name = name;
        this.courseEventDescription = courseEventDescription;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.location = location;
        this.trainers = trainers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseEventDescription() {
        return courseEventDescription;
    }

    public void setCourseEventDescription(String courseEventDescription) {
        this.courseEventDescription = courseEventDescription;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public List<String> getTrainers() {
        return trainers;
    }

    public void setTrainers(List<String> trainers) {
        this.trainers = trainers;
    }
    
    
}
