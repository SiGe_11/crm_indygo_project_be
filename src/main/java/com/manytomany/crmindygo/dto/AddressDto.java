/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

/**
 *
 * @author Balazs
 */
public class AddressDto {
    
    private int zip;
    private String city;
    private String street;
    private int number;
    private String other;

    public AddressDto() {
    }

    public AddressDto(int zip, String city, String street, int number) {
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
    }

    public AddressDto(int zip, String city, String street, int number, String other) {
        this.zip = zip;
        this.city = city;
        this.street = street;
        this.number = number;
        this.other = other;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
