/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

import com.manytomany.crmindygo.entities.Address;
import java.util.List;

/**
 *
 * @author Balazs
 */
public class RegistrationDto {
    
    private String name;
    private String generatedId;
    private String gender;
    private String telephone;
    private Address address;
    private String username;
    private String password;
    private String email;
    private List<String> notAcceptedLocations;

    public RegistrationDto() {
    }

    public RegistrationDto(String name, String generatedId, String gender, String telephone, Address address, String username, String password, String email, List<String> nonAcceptedLocations) {
        this.name = name;
        this.generatedId = generatedId;
        this.gender = gender;
        this.telephone = telephone;
        this.address = address;
        this.username = username;
        this.password = password;
        this.email = email;
        this.notAcceptedLocations = nonAcceptedLocations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeneratedId() {
        return generatedId;
    }

    public void setGeneratedId(String generatedId) {
        this.generatedId = generatedId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getNotAcceptedLocations() {
        return notAcceptedLocations;
    }

    public void setNotAcceptedLocations(List<String> nonAcceptedLocations) {
        this.notAcceptedLocations = nonAcceptedLocations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
