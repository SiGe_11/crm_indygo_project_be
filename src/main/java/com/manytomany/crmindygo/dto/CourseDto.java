/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Márta, Balázs
 */
public class CourseDto {
    
    private String id;
    private List<CourseTypeDto> types;
    private String name;
    private String description;
    private List<CourseEventDto> courseEvents;
    private String discount;
    private int maxStudents;
    private int minStudents;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "hu-HU", timezone = "Europe/Budapest")
    private Date applicationDeadline;
    private int price;
    
    public CourseDto() {
    }

    public CourseDto(String id, List<CourseTypeDto> types, String name, String description, List<CourseEventDto> courseEvents, String discount, int maxStudents, int minStudents, Date applicationDeadline, int price) {
        this.id = id;
        this.types = types;
        this.name = name;
        this.description = description;
        this.courseEvents = courseEvents;
        this.discount = discount;
        this.maxStudents = maxStudents;
        this.minStudents = minStudents;
        this.applicationDeadline = applicationDeadline;
        this.price = price;
    }

    public List<CourseTypeDto> getTypes() {
        return types;
    }

    public void setTypes(List<CourseTypeDto> types) {
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CourseEventDto> getCourseEvents() {
        return courseEvents;
    }

    public void setCourseEvents(List<CourseEventDto> courseEvents) {
        this.courseEvents = courseEvents;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public void setMinStudents(int minStudents) {
        this.minStudents = minStudents;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getApplicationDeadline() {
        return applicationDeadline;
    }

    public void setApplicationDeadline(Date applicationDeadline) {
        this.applicationDeadline = applicationDeadline;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
