package com.manytomany.crmindygo.enums;

/**
 *
 * @author Tomi
 */

// Ezeak a default értékek.
public enum CustomerStatus {
    ENQUIRER, SERIOUS_ENQUIRER, STUDENT, EXALUMNUS    // exalumnus = végzett
}
