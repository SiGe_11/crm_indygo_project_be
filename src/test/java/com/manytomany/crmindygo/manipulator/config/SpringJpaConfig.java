package com.manytomany.crmindygo.manipulator.config;

import com.manytomany.crmindygo.classconverter.CustomDtoConverters;
import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.config.security.MyUserDetailsService;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.manipulator.CourseManipulator;
import com.manytomany.crmindygo.manipulator.CustomerManipulator;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.Location;
import com.manytomany.crmindygo.entities.user.TrainerUser;
import com.manytomany.crmindygo.dto.RegistrationDto;
import com.manytomany.crmindygo.manipulator.InvitationEmailManipulator;
import com.manytomany.crmindygo.manipulator.SimpleQuiryCourse;
import com.manytomany.crmindygo.manipulator.UserManipulator;
import com.manytomany.crmindygo.utilites.mailsender.MailService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.dozer.DozerBeanMapper;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author Balazs
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.manytomany.crmindygo.dao"})
public class SpringJpaConfig {

    private static final Database database = Database.HSQL;


    @Bean
    public CustomDtoConverters cdc(){
        return new CustomDtoConverters();
    }
    
    @Bean
    public CourseManipulator cdbman() {
        return new CourseManipulator();
    }
    
    //FIXME IVDao-t mockolni, vagy valami. + megírni a testet.
//    @Bean
//    public InvitationEmailManipulator iem() {
//        return new InvitationEmailManipulator();
//    }
    
    @Bean
    public Course returnTestCourse() {
        Course testCourse = new Course();
        testCourse.setName("kurzusnév");
        testCourse.setApplicationDeadline(new Date(0));
        testCourse.setDescription("kurzusleírás");
        testCourse.setDiscount("2017-05-01-ig 10% kedvezmény");
        testCourse.setId("c2");
        testCourse.setMaxStudents(20);
        testCourse.setMinStudents(5);
        testCourse.setPrice(65999);
        testCourse.setTypes(returnCourseTypeList());
        List<CourseEvent> courseEventList = returnCourseEventList();
        testCourse.setCourseEvents(courseEventList);
        courseEventList.get(0).setRelatedCourse(testCourse);
        courseEventList.get(1).setRelatedCourse(testCourse);
        
        List<TrainerUser> trainerList = returnTrainerList();
        trainerList.get(0).setMyCourseEvents(courseEventList);
        trainerList.get(1).setMyCourseEvents(courseEventList);
        
        return testCourse;
    }
    
    @Bean
    SimpleQuiryCourse simpleQuiryCourse(){
        return new SimpleQuiryCourse();
    }
    
    @Bean
    public List<CourseEvent> returnCourseEventList() {
        CourseEvent testCourseEvent1 = new CourseEvent();
        testCourseEvent1.setCourseEventDescription("tréningleírás");
        testCourseEvent1.setDateFinish(new Date(0));
        testCourseEvent1.setDateStart(new Date(0));
        testCourseEvent1.setId("e3");
        testCourseEvent1.setLocation(returnLocation());
        testCourseEvent1.setName("tréningnév");
        testCourseEvent1.setTrainers(returnTrainerList());
        
        CourseEvent testCourseEvent2 = new CourseEvent();
        testCourseEvent2.setCourseEventDescription("tréningleírás");
        testCourseEvent2.setDateFinish(new Date(0));
        testCourseEvent2.setDateStart(new Date(0));
        testCourseEvent2.setId("e4");
        testCourseEvent2.setLocation(returnLocation());
        testCourseEvent2.setName("tréningnév");
        testCourseEvent2.setTrainers(returnTrainerList());
        
        List<CourseEvent> courseEventList = new ArrayList<>();
        courseEventList.add(testCourseEvent1);
        courseEventList.add(testCourseEvent2);
        return courseEventList;
    }
    
    @Bean
    public List<TrainerUser> returnTrainerList() {
        List<TrainerUser> trainerList = new ArrayList<>();
        TrainerUser testTrainer1 = new TrainerUser();
        testTrainer1.setId("t1");
        testTrainer1.setName("Name1");
        testTrainer1.setEmail("email1");
        TrainerUser testTrainer2 = new TrainerUser();
        testTrainer2.setId("t2");
        testTrainer2.setName("Name2");
        testTrainer2.setEmail("email2");
        trainerList.add(testTrainer1);
        trainerList.add(testTrainer2);
        return trainerList;
    }
    
    @Bean
    public List<CourseType> returnCourseTypeList() {
        List<CourseType> typeList = new ArrayList<>();
        CourseType type1 = new CourseType();
        type1.setText("típus1");
        CourseType type2 = new CourseType();
        type2.setText("típus2");
        typeList.add(type1);
        typeList.add(type2);
        return typeList;
    }
    
    @Bean
    public Location returnLocation() {
        Location location = new Location();
        Address address = new Address();
        address.setCity("Budapest");
        address.setId("a1");
        address.setNumber(12);
        address.setStreet("utca");
        address.setZip(1234);
        location.setAddress(address);
        location.setId("l1");
        location.setRoom("12 terem");
        List<Location> locationWithThisAddress = new ArrayList<>();
        locationWithThisAddress.add(location);
        address.setLocationsWithThisAddress(locationWithThisAddress);
        return location;
    }
    
    @Bean
    public CustomerManipulator cuman() {
        return new CustomerManipulator();
    }
    
    @Bean
    public DtoToEntity dte(){
        return new DtoToEntity();
    }
    
    @Bean
    public DozerBeanMapper dozerMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        List<String> mappingFileUrls = new ArrayList<>();
        mappingFileUrls.add("dozer-bean-mappings.xml");
        mapper.setMappingFiles(mappingFileUrls);
        return mapper;
    }
    
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean emfb
                = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(dataSource);
        emfb.setJpaVendorAdapter(jpaVendorAdapter);
        emfb.setPackagesToScan("com.manytomany.crmindygo.entities","com.manytomany.crmindygo.entities.security");
        return emfb;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.HSQL);
        adapter.setShowSql(true);
        adapter.setGenerateDdl(true);
        //adapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect");
        adapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");
        return adapter;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        return jpaTransactionManager;
    }
    
    @Bean
    public Address addressToRegistration() {
        Address address = new Address();
        address.setCity("Budapest");
        address.setNumber(1);
        address.setStreet("Kocsi utca");
        address.setZip(1234);
        return address;
    }
    
    @Bean
    @Qualifier("admin")
    public RegistrationDto registrationAdminDto(){
        RegistrationDto dto = new RegistrationDto();
        dto.setAddress(addressToRegistration());
        dto.setEmail("email@email.hu");
        dto.setGender("férfi");
        dto.setGeneratedId("1234");
        dto.setName("name");
        dto.setPassword("password");
        dto.setTelephone("1234");
        dto.setUsername("admin");
        return dto;
    }
    
    @Bean
    @Qualifier("assistant")
    public RegistrationDto registrationAssistantDto(){
        RegistrationDto dto = new RegistrationDto();
        dto.setAddress(addressToRegistration());
        dto.setEmail("email@email.hu");
        dto.setGender("férfi");
        dto.setGeneratedId("1234");
        dto.setName("name");
        dto.setPassword("password");
        dto.setTelephone("1234");
        dto.setUsername("assistant");
        return dto;
    }
    
    @Bean
    @Qualifier("trainer")
    public RegistrationDto registrationTrainerDto(){
        RegistrationDto dto = new RegistrationDto();
        dto.setAddress(addressToRegistration());
        dto.setEmail("email@email.hu");
        dto.setGender("férfi");
        dto.setGeneratedId("1234");
        dto.setName("name");
        dto.setPassword("password");
        dto.setTelephone("1234");
        dto.setUsername("trainer");
        return dto;
    }
    
    @Bean
    public UserManipulator userManipulator() {
        return new UserManipulator();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(myUserDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
    
    @Bean
    public MailService mailService() {
        return Mockito.mock(MailService.class);
    }
    
    @Bean
    public InvitationEmailManipulator iem() {
        return new InvitationEmailManipulator();
    }
    
    @Bean
    public UserDetailsService myUserDetailsService() {
        return new MyUserDetailsService();
    }
}
