/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.manipulator;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.manytomany.crmindygo.dao.UserDao;
import com.manytomany.crmindygo.dto.RegistrationDto;
import com.manytomany.crmindygo.dto.responses.TrainersResponse;
import com.manytomany.crmindygo.manipulator.config.SpringJpaConfig;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Balazs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringJpaConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@Transactional
public class UserManipulatorTest {

    @Autowired
    UserManipulator uman;

    @Autowired
    @Qualifier("admin")
    RegistrationDto adminDto;

    @Autowired
    @Qualifier("assistant")
    RegistrationDto assistantDto;

    @Autowired
    @Qualifier("trainer")
    RegistrationDto trainerDto;

    @Autowired
    UserDao userDao;

    public UserManipulatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of currentUser method, of class UserManipulator.
     */
    //@Test
    public void testCurrentUser() {
    }

    /**
     * Test of saveAdminUserToDb method, of class UserManipulator.
     */
    @Test
    public void testSaveAdminUserToDb() {
        uman.saveAdminUserToDb(adminDto);
        userDao.flush();
        assertNotNull(userDao.findByUsername("admin"));
    }

    /**
     * Test of saveTrainerUserToDb method, of class UserManipulator.
     */
    @Test
    public void testSaveTrainerUserToDb() {
        uman.saveAdminUserToDb(trainerDto);
        userDao.flush();
        assertNotNull(userDao.findByUsername("trainer"));
    }

    /**
     * Test of saveAssistantUserToDb method, of class UserManipulator.
     */
    @Test
    public void testSaveAssistantUserToDb() {
        uman.saveAdminUserToDb(assistantDto);
        userDao.flush();
        assertNotNull(userDao.findByUsername("assistant"));
    }

    /**
     * Test of getTrainers method, of class UserManipulator.
     */
    @DatabaseSetup("userBeforeSave.xml")
    @Test
    public void testGetTrainers() {
        List<TrainersResponse> responseList = uman.getTrainers();
        assertEquals("t1", responseList.get(0).getId());
        assertEquals("Name1", responseList.get(0).getName());
        assertEquals("t2", responseList.get(1).getId());
        assertEquals("Name2", responseList.get(1).getName());
    }

}
