/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.manipulator;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.manytomany.crmindygo.dto.responses.CourseFindResponse;
import com.manytomany.crmindygo.manipulator.config.SpringJpaConfig;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Márta
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringJpaConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@Transactional
public class SimpleFindCourseTest {
    
    @Autowired
    SimpleQuiryCourse simpleQuiryCourse;
    
    public SimpleFindCourseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findCourse method, of class SimpleQuiryCourse.
     */
    @DatabaseSetup("courseAfterSaveNewCourse.xml")
    @Test
    public void testFindCourse() {
        String searchexpression = "kurzus";
        List<CourseFindResponse> resultList = simpleQuiryCourse.findCourse(searchexpression, 1, 10);
        String[] types1 = resultList.get(0).type;
        assertEquals("típus1", types1[0]);
        assertEquals("típus2", types1[1]);
        String[] types2 = resultList.get(1).type;
        assertEquals("típus1", types2[0]);
        assertEquals("típus2", types2[1]);
        assertEquals("kurzusnév", resultList.get(0).name);
        assertEquals("kurzusnév", resultList.get(1).name);
        assertEquals("1970-01-01 01:00:00.0", resultList.get(0).dateStart);
        assertEquals("1970-01-01 01:00:00.0", resultList.get(1).dateStart);
        assertEquals("1970-01-01 01:00:00.0", resultList.get(0).dateFinish);
        assertEquals("1970-01-01 01:00:00.0", resultList.get(1).dateFinish);
        assertEquals("Budapest", resultList.get(0).location);
        assertEquals("Budapest", resultList.get(1).location);
        assertEquals("c1", resultList.get(0).id);
        assertEquals("c2", resultList.get(1).id);
    }
    
}
