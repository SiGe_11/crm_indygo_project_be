/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.manipulator;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.manytomany.crmindygo.manipulator.config.SpringJpaConfig;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.dto.responses.CourseTypesResponse;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Balazs
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringJpaConfig.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@Transactional
public class CourseManipulatorTest {

    @Autowired
    CourseManipulator cdbman;

    @PersistenceContext
    EntityManager em;
    
    @Autowired
    Course course;

    public CourseManipulatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of saveCourseToDb method, of class CourseDbManipulator.
     */
    @DatabaseSetup("courseBeforeSave.xml")
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "courseAfterSaveNewCourse.xml")
    @Test
    public void testSaveCourseToDb() {
        cdbman.saveCourseToDb(course);
        em.flush();
    }

    /**
     * Test of modifyCourse method, of class CourseDbManipulator.
     */
    //@Test
    public void testModifyCourse() {

    }

    /**
     * Test of getCourseTypes method, of class CourseDbManipulator.
     */
    @DatabaseSetup("courseBeforeSave.xml")
    @Test
    public void testGetCourseTypes() {
        List<CourseTypesResponse> response = cdbman.getCourseTypes();
        assertEquals("típus1", response.get(0).getType());
        assertEquals("típus2", response.get(1).getType());
    }

    /**
     * Test of saveNewType method, of class CourseDbManipulator.
     */
    @DatabaseSetup("courseBeforeSave.xml")
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "courseAfterSaveNewType.xml")
    @Test
    public void testSaveNewType() {
        CourseType type = new CourseType();
        type.setText("típus3");
        cdbman.saveNewType(type);
        em.flush();
    }
}
