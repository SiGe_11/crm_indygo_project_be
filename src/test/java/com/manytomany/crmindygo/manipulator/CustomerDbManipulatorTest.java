//package com.manytomany.crmindygo.manipulator;
//
//import com.manytomany.crmindygo.manipulator.CustomerManipulator;
//import com.github.springtestdbunit.DbUnitTestExecutionListener;
//import com.github.springtestdbunit.annotation.DatabaseSetup;
//import com.manytomany.crmindygo.dto.AddressDto;
//import com.manytomany.crmindygo.manipulator.config.SpringJpaConfig;
//import com.manytomany.crmindygo.dto.CustomerDto;
//import com.manytomany.crmindygo.entities.Customer;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// *
// * @author sige
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = SpringJpaConfig.class)
//@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
//        TransactionalTestExecutionListener.class,
//        DbUnitTestExecutionListener.class})
//public class CustomerDbManipulatorTest {
//    
//    @Autowired
//    CustomerManipulator cuman;
//    
//    
//    public CustomerDbManipulatorTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of simplyeFindCustomer method, of class CustomerDbManipulator.
//     */
//    @DatabaseSetup("customer.xml")
//    @Test
//    public void testSimpleFindCustomer() {
//        String searchExp = "Elek";
//        List<CustomerDto> result = cuman.simpleFindCustomer(searchExp,1,10);
//        assertEquals("Teszt Elek", result.get(0).getName());
//    }
//
//    /**
//     * Test of listOfAllCustomers method, of class CustomerDbManipulator.
//     */
//    @DatabaseSetup("customer.xml")
//    @Test
//    public void testListOfAllCustomers() {
//        
//        List<CustomerDto> result = cuman.listOfAllCustomers();
//        assertEquals("Teszt Elek", result.get(0).getName());
//    }
//    
//    @DatabaseSetup("customer.xml")
//    @Test
//    @Transactional
//    public void testDeleteCustomers() {        
//        CustomerDto result = cuman.findCustomerById("2dcd0e87-9bd5-48d8-bfbc-9c7b03719b7b");
//        cuman.deleteCustomer(result);
//        List<CustomerDto> afterDelete = cuman.listOfAllCustomers();
//        assertEquals(afterDelete.size(), 0);
//    }
//    
//    
////    @DatabaseSetup("customer.xml")
////    @Test
////    @Transactional
////    public void testSaveCustomerToDb() {   
////        AddressDto address = new AddressDto(1120, "Budapest", "Asd utca", 12, "második emelet");
////        Customer add = new Customer("Hozzá Adva", "adva@gmail.com", "male", "+123123213", "kindergarden",
////                "alumnus", address.getZip(), address.getCity(), address.getStreet(), 
////                address.getNumber(), address.getOther(), "CTO", new Date(123124), "holdevés", "na");
////        cuman.saveCustomerToDb(add);
////        List<CustomerDto> afterSaved = cuman.simpleFindCustomer("Hozzá", 1, 10);
////
////        assertEquals(afterSaved.get(0).getName(), "Hozzá Adva");
////    }
////    
//    @DatabaseSetup("customer.xml")
//    @Test
//    public void testCompoundFindCustomer() {
//        String searchExpName = "Elek";
//        String searchExpEmail = "gmail";
//        List<CustomerDto> result = cuman.compoundFindCustomer(searchExpName, searchExpEmail,
//                "*", 1, 10);
//        assertEquals("Teszt Elek", result.get(0).getName());
//    }
//}
