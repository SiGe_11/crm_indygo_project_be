/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.manytomany.crmindygo.classconverter.config;

import com.manytomany.crmindygo.classconverter.DtoToEntity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.dozer.DozerBeanMapper;
import org.hibernate.engine.spi.PersistenceContext;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author sige
 */
@Configuration
@ComponentScan(basePackageClasses = DtoToEntity.class)
public class TestConfig {
    
//    @Bean
//    EntityManagerFactory context(){
//        return Mockito.mock(EntityManagerFactory.class);
//    }

    @Bean
    public DozerBeanMapper dozerMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        List<String> mappingFileUrls = new ArrayList<>();
        mappingFileUrls.add("dozer-bean-mappings.xml");
        mapper.setMappingFiles(mappingFileUrls);
        return mapper;
    }
    
}
