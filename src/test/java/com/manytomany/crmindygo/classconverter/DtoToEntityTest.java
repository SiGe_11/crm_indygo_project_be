/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manytomany.crmindygo.classconverter;

import com.manytomany.crmindygo.classconverter.DtoToEntity;
import com.manytomany.crmindygo.dto.CustomerDto;
import com.manytomany.crmindygo.entities.Customer;
import com.manytomany.crmindygo.classconverter.config.TestConfig;
import com.manytomany.crmindygo.dto.AddressDto;
import com.manytomany.crmindygo.dto.CourseDto;
import com.manytomany.crmindygo.dto.CourseEventDto;
import com.manytomany.crmindygo.dto.CourseTypeDto;
import com.manytomany.crmindygo.dto.LocationDto;
import com.manytomany.crmindygo.dto.TrainerDto;
import com.manytomany.crmindygo.entities.Address;
import com.manytomany.crmindygo.entities.Course;
import com.manytomany.crmindygo.entities.CourseEvent;
import com.manytomany.crmindygo.entities.CourseType;
import com.manytomany.crmindygo.entities.Location;
import com.manytomany.crmindygo.entities.user.TrainerUser;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author sige
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class DtoToEntityTest {
    
    @Autowired DtoToEntity dtoToEntity;
    
    public DtoToEntityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of customerConverter method, of class DtoToEntity.
     */
    @Test
    public void testCustomerConverter() {
        CustomerDto dto = new CustomerDto();
        dto.setId(null);

        Customer result = dtoToEntity.customerConverter(dto);
        assertNotNull(result.getId());

    }
    
}
