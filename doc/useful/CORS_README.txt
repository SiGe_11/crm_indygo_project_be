A CORS requestek js oldalon az alábbi módon működtethetők:

Login post (a headers elhagyható)

$.ajax({
  url:'http://localhost:8084/crm_indygo/crm/login',
  type:"POST",
  data:{'username': 'user', 'password':'userPass'},
  dataType:"json",
  //a headers csak azert van itt, hogy biztos legyen CORS preflight request, hogy azt is teszteljuk 
  headers: {
        "My-First-Header":"first value",
        "My-Second-Header":"second value"
  },
  //ennek hatasara kezeli a bongeszo a visszakapott cookie-t
  xhrFields: {
      withCredentials: true
  },
  success: function(data){
    console.log(data)
  }
})

Sikeres login utan mukodik  a tobbi ajax keres:

$.ajax({
  url:'http://localhost:8084/crm_indygo/crm/rest/test',
  type:"POST",
  //a headers csak azert van itt, hogy biztos legyen CORS preflight request, hogy azt is teszteljuk 
  headers: {
        "ize":"ize",
  },
  //ennek hatasara kuldi el a bongeszo a loginnal megszerzett cookiet.
  xhrFields: {
      withCredentials: true
  },
  success: function(data){
    console.log(data)
  }
})



